package com.xurpas.thirdparty.talent.solutions.payload;

import com.xurpas.thirdparty.talent.solutions.model.TypeOfSearch;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchDto {
	
	private String field;
	private TypeOfSearch type;
	private String value;
	

}
