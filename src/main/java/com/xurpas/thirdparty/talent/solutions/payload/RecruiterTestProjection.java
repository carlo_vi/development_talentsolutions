package com.xurpas.thirdparty.talent.solutions.payload;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;

import lombok.Data;

public interface RecruiterTestProjection {
	Long getUser_id();

	String getName();

	Date getLast_login_date();

	Integer getTotal_entries_historical();

	// @Value("#{@RecruiterRepository.test(target)}")
	Integer getTotal_entries_this_week();

}
