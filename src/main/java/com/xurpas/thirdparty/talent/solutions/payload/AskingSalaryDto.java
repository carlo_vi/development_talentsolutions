package com.xurpas.thirdparty.talent.solutions.payload;

import java.math.BigInteger;

import lombok.Data;

@Data
public class AskingSalaryDto {
private Integer techId;
private String techName;
private BigInteger n1;
private Double value1;
private BigInteger n2;
private Double value2;
private BigInteger n3;
private Double value3;
private BigInteger n4;
private Double value4;
}
