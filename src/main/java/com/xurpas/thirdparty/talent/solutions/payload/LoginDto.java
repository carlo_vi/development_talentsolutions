package com.xurpas.thirdparty.talent.solutions.payload;

import java.util.Date;

import lombok.Data;

@Data
public class LoginDto {
	private long user_id;
    private String email;
    private String password;
    private Date last_login_date = new Date();
    
    
	public long getUser_id() {
		return user_id;
	}
	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getLast_login_date() {
		return last_login_date;
	}
	public void setLast_login_date(Date last_login_date) {
		this.last_login_date = last_login_date;
	}
	
    
}