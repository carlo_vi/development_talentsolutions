package com.xurpas.thirdparty.talent.solutions.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xurpas.thirdparty.talent.solutions.model.CandidateFile;

@Repository
public interface CandidateFileRepository extends JpaRepository<CandidateFile, Integer> {

	CandidateFile findById(String id);

	List<CandidateFile> findByFolderId(String id);

	CandidateFile findCandidateFileById(Integer id);

	@Query(value = "SELECT * FROM candidate_file f JOIN  candidates c ON f.candidate_id=c.candidate_id WHERE c.candidate_id=:candidateId LIMIT 1",
			nativeQuery = true)
	CandidateFile getFileByCandidateId(@Param("candidateId") Long candidateId);
	
	
}
