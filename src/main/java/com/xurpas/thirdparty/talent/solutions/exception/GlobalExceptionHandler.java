package com.xurpas.thirdparty.talent.solutions.exception;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.google.api.client.googleapis.json.GoogleJsonResponseException;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		Map<String, String> errors = new HashMap<>();
		ex.getBindingResult().getAllErrors().forEach((error) -> {

			String fieldName = ((FieldError) error).getField();
			String message = error.getDefaultMessage();
			errors.put(fieldName, message);
		});

		return new ResponseEntity<Object>(errors, HttpStatus.BAD_REQUEST);
	
	}
	/*@ExceptionHandler(value = { GoogleJsonResponseException.class })
	public ResponseEntity<String> handleGoogleUnauthorized(GoogleJsonResponseException e) {
		HttpStatus badRequest = HttpStatus.BAD_REQUEST;

		return new ResponseEntity<String>("Sign in to google account",
				badRequest);
	}*/
	
	@ExceptionHandler(value = { CandidateEmptyRequestException.class })
	public ResponseEntity<String> handleEmptyInput(CandidateEmptyRequestException e) {
		HttpStatus badRequest = HttpStatus.BAD_REQUEST;

		return new ResponseEntity<String>("first name, middle name, last name, email, and mobile number cant be empty",
				badRequest);
	}

	@ExceptionHandler(value = { NoSuchElementException.class })
	public ResponseEntity<String> handleNoSuchElement(NoSuchElementException e) {

		return new ResponseEntity<String>("no value is present in DB, please change your request",
				HttpStatus.NOT_FOUND);
	}

//	@ExceptionHandler(value = { DataIntegrityViolationException.class })
//	public ResponseEntity<String> handleDataIntegrityViolationException(DataIntegrityViolationException e) {
//
//		return new ResponseEntity<String>(e.getMostSpecificCause().toString(), HttpStatus.BAD_REQUEST);
//	}

	
//	@Override
//	protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex,
//			HttpHeaders headers, HttpStatus status, WebRequest request) {
//		return new ResponseEntity<Object>("change http method", HttpStatus.NOT_FOUND);
//	}
}
