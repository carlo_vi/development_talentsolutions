package com.xurpas.thirdparty.talent.solutions.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
public class MyAppConfig implements WebMvcConfigurer {
	@Value("${allowed.origins}")
	private String[] theAllowedOrigins;
	

	@Override
	public void addCorsMappings(CorsRegistry cors) {
		cors.addMapping("/**").
		allowedOrigins("http://localhost:8089","http://localhost:4200","http://localhost:8080")
		.allowCredentials(true)
		;
	}

}
