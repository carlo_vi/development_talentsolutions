package com.xurpas.thirdparty.talent.solutions.payload;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.xurpas.thirdparty.talent.solutions.payload.AddCandidateDto.FilesDto;

import lombok.Data;

@Data
public class GetCandidateDto {
	private String firstName;
	private String lastName;
	private String middleName;
	private String email;
	private String mobileNo;
	private String source;
	private String owner;
	private String school;
	private String jobType;
	private String jobTitle;
	private Integer totalYearsExp;
	private Integer yearGraduated;
	private String course;
	private String address;
	private String currentStatus;
	private Date dateCreated;
	private Double askingSalary;
	private Double currentSalary;
	private List<FilesDto> files= new ArrayList<>();
	private List<GetTechnologyDto> technologies = new ArrayList<>();

	private List<GetNotesDto> notes = new ArrayList<>();
	@Data
	public class GetNotesDto{
		private String status;
		private String note;
		private String user;
		private Date dateCreated;
	}
	
	@Data
	public class GetTechnologyDto {
		public Integer id;
		public Integer technologyId;
		public String technologyName;
		public Integer yearExperience;

		
	}
	@Data
	public class FilesDto {
		private String folderId;
		private String fileId;
		private String link;
		private String fileType;
	}
	
}
