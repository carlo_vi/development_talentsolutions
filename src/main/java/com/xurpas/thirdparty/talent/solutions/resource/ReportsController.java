package com.xurpas.thirdparty.talent.solutions.resource;

import java.math.BigInteger;
import java.time.Duration;
import java.time.Instant;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xurpas.thirdparty.talent.solutions.exception.ResponseHandler;
import com.xurpas.thirdparty.talent.solutions.model.CandidateTechnology;
import com.xurpas.thirdparty.talent.solutions.payload.AskingSalaryProjection;
import com.xurpas.thirdparty.talent.solutions.payload.FilterDate;
import com.xurpas.thirdparty.talent.solutions.payload.ReportDto;
import com.xurpas.thirdparty.talent.solutions.payload.ReportFilter;
import com.xurpas.thirdparty.talent.solutions.payload.ReportFilterDto;
import com.xurpas.thirdparty.talent.solutions.payload.SourceDto;
import com.xurpas.thirdparty.talent.solutions.payload.SourcesProjection;
import com.xurpas.thirdparty.talent.solutions.payload.TechnologyProjection;
import com.xurpas.thirdparty.talent.solutions.repository.CandidateRepository;
import com.xurpas.thirdparty.talent.solutions.repository.CandidateTechnologyRepository;
import com.xurpas.thirdparty.talent.solutions.service.ReportsService;

@Controller
public class ReportsController {

	@Autowired
	 public ReportsService service;

	@Autowired
	public CandidateTechnologyRepository repo;

	@Autowired
	public CandidateRepository candidateRepo;

	/*
	 * @GetMapping("/barChart") public String getTechnologyJava(Model model) {
	 * List<Technology> jAva =
	 * service.listAll().stream().map(x->x.getTechnology()).collect(Collectors.
	 * toList()); model.addAttribute("Java", jAva);
	 * 
	 * return "barChart";
	 * 
	 * }
	 */
	/*
	 * @RequestMapping("/multiplelinechart") public ResponseEntity<?>
	 * getDataForMultipleLine() { List<CandidateTechnology> dataList =
	 * repo.findAll(); Map<String, List<CandidateTechnology>> mappedData = new
	 * HashMap<>(); for(CandidateTechnology data : dataList) {
	 * 
	 * if(mappedData.containsKey(data.getTechnology())) {
	 * mappedData.get(data.getTechnology()).add(data); }else {
	 * List<CandidateTechnology> tempList = new ArrayList<CandidateTechnology>();
	 * tempList.add(data); mappedData.put(data.getTechnology(), tempList); }
	 * 
	 * } return new ResponseEntity<>(mappedData, HttpStatus.OK); }
	 */

	@RequestMapping("/chart/source")
	public ResponseEntity<?> getSource(@RequestBody ReportDto reportDto) {
		int value = reportDto.getValue();
		LocalDate fromYears = LocalDate.now().minusYears(value);
		LocalDate fromMonths = LocalDate.now().minusMonths(value);
		LocalDate fromDay = LocalDate.now().minusDays(value);
		Date year = Date.from(fromYears.atStartOfDay(ZoneId.systemDefault()).toInstant());
		Date month = Date.from(fromMonths.atStartOfDay(ZoneId.systemDefault()).toInstant());
		Date day = Date.from(fromDay.atStartOfDay(ZoneId.systemDefault()).toInstant());

		List<SourcesProjection> databaseData1 = candidateRepo.findSource(day, month, year, reportDto.getFilter(),
				FilterDate.day, FilterDate.year, FilterDate.month);

		return new ResponseEntity<>(databaseData1, HttpStatus.OK);

	}
	
	@PostMapping("/chart/sources")
	public ResponseEntity<?> getSourceReport(@RequestBody ReportFilterDto reportFilterDto) {
		try {
			
		return ResponseHandler.generateResponseWithEntity("Successful", HttpStatus.OK, service.getSourceReport(reportFilterDto));
				
		}catch(NullPointerException e) {
			return new ResponseEntity<>("Please fill possible values", HttpStatus.BAD_REQUEST);
		}
	
	}

	@PostMapping("/chart/technologies")
	public ResponseEntity<?> getTechnologyReport(@RequestBody ReportFilterDto reportFilterDto) {
		try {
			return ResponseHandler.generateResponseWithEntity("Successful", HttpStatus.OK,service.getTechnologyReport(reportFilterDto));	
			
		}
		catch(NullPointerException e) {
			 return ResponseHandler.generateResponse("There is a missing field for request!", HttpStatus.BAD_REQUEST);
		}
		
	}
	
	@PostMapping("/chart/askingsalaries")
	public  ResponseEntity<?> getAskingSalarayReport(@RequestBody ReportFilterDto reportFilterDto) {
		try {
			return ResponseHandler.generateResponseWithEntity("Successful", HttpStatus.OK,service.getAskingSalaryReport(reportFilterDto));
			
		}catch(NullPointerException e) {
			return new ResponseEntity<>("Please fill possible values", HttpStatus.BAD_REQUEST);
		}
		
	

	}
	@RequestMapping("/chart/askingsalary")
	public ResponseEntity<?> getAskingSalary(@RequestBody ReportDto reportDto) {
		int value = reportDto.getValue();
		LocalDate fromYears = LocalDate.now().minusYears(value);
		LocalDate fromMonths = LocalDate.now().minusMonths(value);
		LocalDate fromDay = LocalDate.now().minusDays(value);
		Date year = Date.from(fromYears.atStartOfDay(ZoneId.systemDefault()).toInstant());
		Date month = Date.from(fromMonths.atStartOfDay(ZoneId.systemDefault()).toInstant());
		Date day = Date.from(fromDay.atStartOfDay(ZoneId.systemDefault()).toInstant());
		List<AskingSalaryProjection> databaseData1 = candidateRepo.findAskingSalary(day, month, year,
				reportDto.getFilter(), FilterDate.day, FilterDate.year, FilterDate.month);
		return new ResponseEntity<>(databaseData1, HttpStatus.OK);
	}

	@RequestMapping("/chart/technology")
	public ResponseEntity<?> getTechnology(@RequestBody ReportDto reportDto) {
		int value = reportDto.getValue();
		LocalDate fromYears = LocalDate.now().minusYears(value);
		LocalDate fromMonths = LocalDate.now().minusMonths(value);
		LocalDate fromDay = LocalDate.now().minusDays(value);
		Date year = Date.from(fromYears.atStartOfDay(ZoneId.systemDefault()).toInstant());
		Date month = Date.from(fromMonths.atStartOfDay(ZoneId.systemDefault()).toInstant());
		Date day = Date.from(fromDay.atStartOfDay(ZoneId.systemDefault()).toInstant());
		List<TechnologyProjection> databaseData1 = candidateRepo.findTechnology(day, month, year, reportDto.getFilter(),
				FilterDate.day, FilterDate.year, FilterDate.month);
		return new ResponseEntity<>(databaseData1, HttpStatus.OK);

	}
	

}
