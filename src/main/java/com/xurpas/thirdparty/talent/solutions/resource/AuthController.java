package com.xurpas.thirdparty.talent.solutions.resource;



import com.xurpas.thirdparty.talent.solutions.exception.ResponseHandler;
import com.xurpas.thirdparty.talent.solutions.model.Config;
import com.xurpas.thirdparty.talent.solutions.model.Role;


import com.xurpas.thirdparty.talent.solutions.model.User;
import com.xurpas.thirdparty.talent.solutions.payload.LoginDto;
import com.xurpas.thirdparty.talent.solutions.payload.SignUpDto;
import com.xurpas.thirdparty.talent.solutions.repository.ConfigRepository;
import com.xurpas.thirdparty.talent.solutions.repository.RoleRepository;
import com.xurpas.thirdparty.talent.solutions.repository.UserRepository;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



@RestController
@RequestMapping("/user")
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired

    private UserRepository userRepository;


    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @Autowired
    private ConfigRepository configRepo;
   

    
  
	@PostMapping("/login")
    public ResponseEntity<Object> authenticateUser(@RequestBody LoginDto loginDto,HttpServletRequest request){
		 try {
			 if(loginDto.getEmail().equals("") || loginDto.getPassword().equals("")){
		        	return ResponseHandler.generateResponse("Please fill all possible value", HttpStatus.BAD_REQUEST);
				
		        }else {
		        	 Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
		                     loginDto.getEmail(), loginDto.getPassword()));
		        	 SecurityContextHolder.getContext().setAuthentication(authentication);

		             User user = userRepository.findUserByEmail(authentication.getName()) ;

		             Date nowdate = new Date();
		             user.setLast_login_date(nowdate);
		             
		             userRepository.save(user);
		             
		         
		             String msg = authentication.getName();
		     		List<String> messages = (List<String>) request.getSession().getAttribute("MY_SESSION_MESSAGES");
		     		if (messages == null) {
		     			messages = new ArrayList<>();
		     			request.getSession().setAttribute("MY_SESSION_MESSAGES", messages);
		     		}
		     		messages.add(msg);
		     		request.getSession().setAttribute("MY_SESSION_MESSAGES", messages);
		     		
		     		request.getSession().setAttribute("email", messages.get(messages.size()-1).toString());
		     		 String test=request.getSession().getAttribute("MY_SESSION_MESSAGES").toString(); 
		     		return ResponseHandler.generateResponse(test+" User signed-in successfully!.", HttpStatus.OK);
		             
		            
		        }
		 }catch (NullPointerException e) {
			 return ResponseHandler.generateResponse("There is a missing field for request!", HttpStatus.BAD_REQUEST);
		 }
		
     
    }
	 @GetMapping("/getconfig")
    public String getConfig() {
    	return configRepo.findByName("AS_YTDVsLY_1_to");
    }
  
    @GetMapping("/getString")
    public ResponseEntity<String> StringResponse() { 
    	return new ResponseEntity<>("Nice", HttpStatus.OK);
    }
    @GetMapping("/getSession")
    public ResponseEntity<String> getUserSession(HttpServletRequest request){
    //List<String> list =(List<String>) request.getSession().getAttribute("MY_SESSION_MESSAGES");
    	//return new ResponseEntity<>( request.getSession().getAttribute("MY_SESSION_MESSAGES").toString(), HttpStatus.OK);
    	return new ResponseEntity<>( request.getUserPrincipal().getName().toString(), HttpStatus.OK);
    	 
    	//return new ResponseEntity<>( request.getSession().getAttribute("email").toString(), HttpStatus.OK);
    }
    
    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@RequestBody SignUpDto signUpDto){

        // add check for username exists in a DB
        /*if(userRepository.existsByUsername(signUpDto.getUsername())){
            return new ResponseEntity<>("Username is already taken!", HttpStatus.BAD_REQUEST);
        }*/

        // add check for email exists in DB
    	try {
//    		  boolean hr=!signUpDto.getRoleName().equals("HR");
//    		  boolean tr=!signUpDto.getRoleName().equals("Technical Recruiter");
    		  if(userRepository.existsByEmail(signUpDto.getEmail())){
    	         
    	           
    	            return ResponseHandler.generateResponse("Email is already taken!.", HttpStatus.BAD_REQUEST);
    	        } else if (signUpDto.getFirstName().equals("") || signUpDto.getLastName().equals("")
    	        		|| signUpDto.getEmail().equals("") || signUpDto.getPassword().equals("") || signUpDto.getRoleName().equals("")) {
    	        	 return ResponseHandler.generateResponse("Please fill all possible value", HttpStatus.BAD_REQUEST);
    	        } 
//    	        else if ( hr&&tr ) {
//    	        	 return ResponseHandler.generateResponse("Wrong Role Name.", HttpStatus.BAD_REQUEST);
//    	        }
    	        else {
    	        	 // create user object

    	            User user = new User();

    	            user.setFirstName(signUpDto.getFirstName());
    	            user.setLastName(signUpDto.getLastName());
    	            user.setEmail(signUpDto.getEmail());
    	            user.setPassword(passwordEncoder.encode(signUpDto.getPassword()));
    	            
    	            Role role1 = roleRepository.findByroleName("HR").get();
    	            Role role2 = roleRepository.findByroleName("Technical Recruiter").get();
    	            	//Try to set role by putting payload in this parameter.
    	            if(signUpDto.getRoleName().equals("HR")){
    	            	user.setRoles(Collections.singleton(role1));
    	            }else if(signUpDto.getRoleName().equals("Technical Recruiter")) {
    	            	user.setRoles(Collections.singleton(role2));
    	            }else {
    	            	return ResponseHandler.generateResponse("Wrong Role Name.", HttpStatus.BAD_REQUEST);
    	      	      
    	            }
    	            
    	            

    	            userRepository.save(user);
    	            return ResponseHandler.generateResponse("User registered successfully", HttpStatus.OK);
    	        }

    	} catch (NullPointerException e) {
    		 return ResponseHandler.generateResponse("There is a missing field for request!", HttpStatus.BAD_REQUEST);
    	}
    	
      
       
       

    }
    @PostMapping("/logout")
    public ResponseEntity<?> getLogoutPage(HttpServletRequest request, HttpServletResponse response){

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null)
            new SecurityContextLogoutHandler().logout(request, response, authentication);

        return ResponseHandler.generateResponse("Successfully Logout", HttpStatus.OK);
    }
    /*
    @PostMapping("/register/TechnicalRecruiter")
    public ResponseEntity<?> registerUser2(@RequestBody SignUpDto signUpDto){

         add check for username exists in a DB
        if(userRepository.existsByUsername(signUpDto.getUsername())){
            return new ResponseEntity<>("Username is already taken!", HttpStatus.BAD_REQUEST);
        }

        // add check for email exists in DB
        if(userRepository.existsByEmail(signUpDto.getEmail())){
            return new ResponseEntity<>("Email is already taken!", HttpStatus.BAD_REQUEST);
        }


        // create user object
        User user = new User();

        user.setFirstName(signUpDto.getFirstName());
        user.setLastName(signUpDto.getLastName());
        user.setEmail(signUpDto.getEmail());
        user.setPassword(passwordEncoder.encode(signUpDto.getPassword()));
        
        Role role1 = roleRepository.findByroleName("Technical Recruiter").get();
       
        	user.setRoles(Collections.singleton(role1));
        
        
        

        userRepository.save(user);

        return new ResponseEntity<>("User registered successfully", HttpStatus.OK);

    }
*/
}