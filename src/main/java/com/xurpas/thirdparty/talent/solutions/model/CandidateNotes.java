package com.xurpas.thirdparty.talent.solutions.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "candidate_notes")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CandidateNotes {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int note_id;


	@ManyToOne
	@JoinColumn(name = "user_id")
	private User userAccount;

	@ManyToOne
	@JoinColumn(name = "status_id")
	private CandidateStatus candidateStatus;

	@Column(name = "note",length = 1000)
	private String note;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_created")
	private Date dateCreated;
	
	@PrePersist
	private void onCreate() {
		dateCreated = new Date();
	}

	public CandidateNotes(User userAccount, CandidateStatus candidateStatus, String note) {
		this.userAccount = userAccount;
		this.candidateStatus = candidateStatus;
		this.note = note;
	}


	
	
	
}
