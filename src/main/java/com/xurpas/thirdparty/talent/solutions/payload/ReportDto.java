package com.xurpas.thirdparty.talent.solutions.payload;

import lombok.Data;

@Data
public class ReportDto {
Integer value;
FilterDate filter;
}
