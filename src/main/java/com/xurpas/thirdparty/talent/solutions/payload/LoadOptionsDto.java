package com.xurpas.thirdparty.talent.solutions.payload;

import java.util.List;

import com.xurpas.thirdparty.talent.solutions.model.CandidateSource;
import com.xurpas.thirdparty.talent.solutions.model.CandidateStatus;
import com.xurpas.thirdparty.talent.solutions.model.Technology;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoadOptionsDto {

	List<CandidateSource> source;
	List<Technology> technology;
	List<CandidateStatus> status;
	List<String> jobTitle;
}
