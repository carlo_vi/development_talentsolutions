package com.xurpas.thirdparty.talent.solutions.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FileItem {
	private static final long serialVersionUID = 1l;
	private String name;
	private String id;
	private String mime;
	private String driveId;
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


}
