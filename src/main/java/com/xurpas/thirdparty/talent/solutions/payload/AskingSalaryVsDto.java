package com.xurpas.thirdparty.talent.solutions.payload;

import lombok.Data;

@Data
public class AskingSalaryVsDto {
	private Integer id;
	private String technology_name;
	private Double growth02;
	private Double growth23;
	private Double growth34;
	private Double growth5;
}
