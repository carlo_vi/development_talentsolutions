package com.xurpas.thirdparty.talent.solutions.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import org.springframework.stereotype.Repository;

import com.xurpas.thirdparty.talent.solutions.model.Candidates;
import com.xurpas.thirdparty.talent.solutions.model.TypeOfSearch;
import com.xurpas.thirdparty.talent.solutions.payload.SearchResultProjection;

import com.xurpas.thirdparty.talent.solutions.payload.AskingSalaryProjection;
import com.xurpas.thirdparty.talent.solutions.payload.FilterDate;

import com.xurpas.thirdparty.talent.solutions.payload.GetCandidateProjection;
import com.xurpas.thirdparty.talent.solutions.payload.RecruiterTestProjection;
import com.xurpas.thirdparty.talent.solutions.payload.ReportFilter;
import com.xurpas.thirdparty.talent.solutions.payload.SourcesProjection;
import com.xurpas.thirdparty.talent.solutions.payload.TechnologyProjection;

@Repository
public interface CandidateRepository extends JpaRepository<Candidates, Long> {

	@Query("SELECT c FROM Candidates c WHERE c.candidate_id=:id")
	GetCandidateProjection getCandidateById(@Param("id") Long id);

	
	boolean existsById( Long id);
	boolean existsByEmail(String email);
	boolean existsByMobileNo (String mobileNo);
	
	@Query("SELECT c FROM Candidates c WHERE (:filter=:day AND c.dateCreated >:intervalDay) OR "
			+ "(:filter=:month AND c.dateCreated >:intervalMonth) OR (:filter=:year AND c.dateCreated >:intervalYear)")
	public List<SourcesProjection> findSource(@Param("intervalDay") Date intervalDay,
			@Param("intervalMonth") Date intervalMonth, @Param("intervalYear") Date intervalYear,
			@Param("filter") FilterDate filterEnum, @Param("day") FilterDate day, @Param("year") FilterDate year,
			@Param("month") FilterDate month);

	@Query(value = "select sources.source_name as sourceName,count(sources.source_name) as count "
			+ "from candidates c1 join candidate_source sources  " + " on c1.source_id = sources.source_id WHERE c1.date_created > "
			+ "CASE WHEN :reportFilter=:ytd THEN "
			+ "NOW() - INTERVAL '1 YEAR' "
			+ "ELSE NOW() - INTERVAL '60 DAYS' "
			+ "END "
			+ "group by sources.source_name ", nativeQuery = true)
	public List<Object[]> getSourceReport(@Param("ytd")ReportFilter ytd,@Param("reportFilter") ReportFilter reportFilter);
	@Query(value = "select s.source_name, cy.count as current_year,py.count as previous_year,(COALESCE(cy.count,0)-COALESCE(py.count,0)) as growth from candidate_source s LEFT JOIN "
			+ "(SELECT source.source_name as sourceName,COUNT(source.source_name)AS count FROM candidates c "
			+ "JOIN candidate_source source ON c.source_id=source.source_id WHERE c.date_created > "
			+ "NOW() - INTERVAL '1 YEAR' "
			+ "GROUP BY source.source_name)cy ON s.source_name=cy.sourceName LEFT JOIN "
			+ ""
			+ "(SELECT source.source_name as sourceName,COUNT(source.source_name)AS count FROM candidates c "
			+ "JOIN candidate_source source ON c.source_id=source.source_id WHERE c.date_created < "
			+ "NOW() - INTERVAL '1 YEAR' AND c.date_created >NOW() - INTERVAL '2 YEAR' "
			+ "GROUP BY source.source_name)py ON s.source_name=py.sourceName; ", nativeQuery = true)
	public List<Object[]> getSourceVsReport();
	

	@Query(value = "select t2.technology_name,count(t2.technology_name) from candidate_technology t1 "
			+ "join technologies t2 on t1.technology_id=t2.technology_id "
			+ "join candidates as candidate on t1.candidate_id = candidate.candidate_id WHERE candidate.date_created > "
			+ "CASE WHEN :reportFilter=:ytd THEN "
			+ "NOW() - INTERVAL '1 YEAR' "
			+ "ELSE NOW() - INTERVAL '60 DAYS' "
			+ "END "
			+ " group by t2.technology_name ; ", nativeQuery = true)
	public List<Object[]> getTechnologyReport(@Param("ytd")ReportFilter ytd,@Param("reportFilter") ReportFilter reportFilter);
	@Query(value = "SELECT t.technology_name,cy.count AS current_year,py.count AS previous_year,(COALESCE(cy.count,0)-COALESCE(py.count,0))AS growth FROM technologies t LEFT JOIN "
			+ "(SELECT t.technology_name,COUNT(t.technology_name) AS count FROM candidates c "
			+ "JOIN candidate_technology ct ON ct.candidate_id=c.candidate_id "
			+ "JOIN technologies t ON t.technology_id=ct.technology_id "
			+ "WHERE c.date_created > NOW() - INTERVAL '1 YEAR' "
			+ "GROUP BY t.technology_name)cy ON t.technology_name=cy.technology_name LEFT JOIN "
			+ "(SELECT t.technology_name,COUNT(t.technology_name)AS count FROM candidates c "
			+ "JOIN candidate_technology ct ON ct.candidate_id=c.candidate_id "
			+ "JOIN technologies t ON t.technology_id=ct.technology_id "
			+ "WHERE c.date_created < "
			+ "NOW() - INTERVAL '1 YEAR' AND c.date_created >NOW() - INTERVAL '2 YEAR' "
			+ "GROUP BY t.technology_name)py ON t.technology_name=py.technology_name;", nativeQuery = true)
	public List<Object[]> getTechnologyVsReport();

	@Query(value = "SELECT t.technology_id,t.technology_name,t1.number_Of_Employees AS n1,t1.Average avg_0_to_2,t2.number_Of_Employees AS n2,t2.Average AS avg_2_to_3, "
			+ "t3.number_Of_Employees AS n3,t3.Average AS avg_3_to_4,t4.number_Of_Employees AS n4, t4.Average AS avg_greater_than_5 FROM technologies t LEFT JOIN "
			+ " "
			+ "(SELECT t.technology_name AS tech,COUNT(t.technology_name)AS number_Of_Employees,AVG(c.asking_salary)AS Average FROM candidates c "
			+ "JOIN candidate_technology ct ON c.candidate_id=ct.candidate_id "
			+ "JOIN technologies t ON t.technology_id=ct.technology_id "
			+ "WHERE (ct.year_exp>=0 AND ct.year_exp <=2) AND c.date_created > " 
			+ "CASE WHEN :reportFilter=:ytd THEN "
			+ "NOW() - INTERVAL '1 YEAR' " 
			+ "ELSE NOW() - INTERVAL '60 DAYS' " 
			+ "END "
			+ "GROUP BY t.technology_name)t1 ON t1.tech=t.technology_name LEFT JOIN " 
			+ " "
			+ "(SELECT t.technology_name AS tech,COUNT(t.technology_name)as number_Of_Employees,AVG(c.asking_salary)as Average FROM candidates c "
			+ "JOIN candidate_technology ct ON c.candidate_id=ct.candidate_id "
			+ "JOIN technologies t ON t.technology_id=ct.technology_id "
			+ "WHERE (ct.year_exp>=2 AND ct.year_exp <=3) AND c.date_created > " 
			+ "CASE WHEN :reportFilter=:ytd THEN "
			+ "NOW() - INTERVAL '1 YEAR' " + "ELSE NOW() - INTERVAL '60 DAYS' " 
			+ "END "
			+ "GROUP BY t.technology_name)t2 ON t2.tech=t.technology_name LEFT JOIN " 
			+ " "
			+ "(SELECT t.technology_name AS tech,COUNT(t.technology_name)as number_Of_Employees,AVG(c.asking_salary)as Average FROM candidates c "
			+ "JOIN candidate_technology ct ON c.candidate_id=ct.candidate_id "
			+ "JOIN technologies t ON t.technology_id=ct.technology_id "
			+ "WHERE (ct.year_exp>=3 AND ct.year_exp <=4) AND c.date_created > " 
			+ "CASE WHEN :reportFilter=:ytd THEN "
			+ "NOW() - INTERVAL '1 YEAR' " + "ELSE NOW() - INTERVAL '60 DAYS' " 
			+ "END "
			+ "GROUP BY t.technology_name)t3 ON t3.tech=t.technology_name LEFT JOIN " 
			+ " "
			+ "(SELECT t.technology_name AS tech,COUNT(t.technology_name)as number_Of_Employees,AVG(c.asking_salary)as Average FROM candidates c "
			+ "JOIN candidate_technology ct ON c.candidate_id=ct.candidate_id "
			+ "JOIN technologies t ON t.technology_id=ct.technology_id " 
			+ "WHERE ct.year_exp>=5 AND c.date_created > "
			+ "CASE WHEN :reportFilter=:ytd THEN " + "NOW() - INTERVAL '1 YEAR' " 
			+ "ELSE NOW() - INTERVAL '60 DAYS' "
			+ "END " 
			+ "GROUP BY t.technology_name)t4 ON t4.tech=t.technology_name ", nativeQuery = true)
	List<Object[]> getAskingSalaryReport(@Param("ytd")ReportFilter ytd,@Param("reportFilter") ReportFilter reportFilter);
	@Query(value = "SELECT t.technology_id,t.technology_name,t1.number_Of_Employees AS n1,t1.Average avg_0_to_2,t2.number_Of_Employees AS n2,t2.Average AS avg_2_to_3, "
			+ "t3.number_Of_Employees AS n3,t3.Average AS avg_3_to_4,t4.number_Of_Employees AS n4, t4.Average AS avg_greater_than_5 FROM technologies t LEFT JOIN "
			+ " "
			+ "(SELECT t.technology_name AS tech,COUNT(t.technology_name)AS number_Of_Employees,AVG(c.asking_salary)AS Average FROM candidates c "
			+ "JOIN candidate_technology ct ON c.candidate_id=ct.candidate_id "
			+ "JOIN technologies t ON t.technology_id=ct.technology_id "
			+ "WHERE (ct.year_exp>=:param1from AND ct.year_exp <=:param1to) AND c.date_created > " 
			+ "CASE WHEN :reportFilter=:ytd THEN "
			+ "NOW() - INTERVAL '1 YEAR' " 
			+ "ELSE NOW() - INTERVAL '60 DAYS' " 
			+ "END "
			+ "GROUP BY t.technology_name)t1 ON t1.tech=t.technology_name LEFT JOIN " 
			+ " "
			+ "(SELECT t.technology_name AS tech,COUNT(t.technology_name)as number_Of_Employees,AVG(c.asking_salary)as Average FROM candidates c "
			+ "JOIN candidate_technology ct ON c.candidate_id=ct.candidate_id "
			+ "JOIN technologies t ON t.technology_id=ct.technology_id "
			+ "WHERE (ct.year_exp>=:param2from AND ct.year_exp <=:param2to) AND c.date_created > " 
			+ "CASE WHEN :reportFilter=:ytd THEN "
			+ "NOW() - INTERVAL '1 YEAR' " + "ELSE NOW() - INTERVAL '60 DAYS' " 
			+ "END "
			+ "GROUP BY t.technology_name)t2 ON t2.tech=t.technology_name LEFT JOIN " 
			+ " "
			+ "(SELECT t.technology_name AS tech,COUNT(t.technology_name)as number_Of_Employees,AVG(c.asking_salary)as Average FROM candidates c "
			+ "JOIN candidate_technology ct ON c.candidate_id=ct.candidate_id "
			+ "JOIN technologies t ON t.technology_id=ct.technology_id "
			+ "WHERE (ct.year_exp>=:param3from AND ct.year_exp <=:param3to) AND c.date_created > " 
			+ "CASE WHEN :reportFilter=:ytd THEN "
			+ "NOW() - INTERVAL '1 YEAR' " + "ELSE NOW() - INTERVAL '60 DAYS' " 
			+ "END "
			+ "GROUP BY t.technology_name)t3 ON t3.tech=t.technology_name LEFT JOIN " 
			+ " "
			+ "(SELECT t.technology_name AS tech,COUNT(t.technology_name)as number_Of_Employees,AVG(c.asking_salary)as Average FROM candidates c "
			+ "JOIN candidate_technology ct ON c.candidate_id=ct.candidate_id "
			+ "JOIN technologies t ON t.technology_id=ct.technology_id " 
			+ "WHERE ct.year_exp>=:param4atleast AND c.date_created > "
			+ "CASE WHEN :reportFilter=:ytd THEN " + "NOW() - INTERVAL '1 YEAR' " 
			+ "ELSE NOW() - INTERVAL '60 DAYS' "
			+ "END " 
			+ "GROUP BY t.technology_name)t4 ON t4.tech=t.technology_name ", nativeQuery = true)
	List<Object[]> testgetAskingSalaryReport(@Param("ytd")ReportFilter ytd,@Param("reportFilter") ReportFilter reportFilter,
			@Param("param1from") Integer param1from,@Param("param1to") Integer param1to,
			@Param("param2from") Integer param2from,@Param("param2to") Integer param2to,
			@Param("param3from") Integer param3from,@Param("param3to") Integer param3to,
			@Param("param4atleast") Integer param4atleast);

	@Query(value = "SELECT t.technology_id AS id,t.technology_name AS tech, "
			+ "(t1.Average-t1P.Average)AS growth02, "
			+ "(t2.Average-t2P.Average)AS growth23, "
			+ "(t3.Average-t3P.Average)AS growth34, "
			+ "(t4.Average-t4P.Average)AS growth5 "
			+ "FROM technologies t LEFT JOIN "
			+ "(SELECT t.technology_name AS tech,AVG(c.asking_salary)AS Average FROM candidates c "
			+ "JOIN candidate_technology ct ON c.candidate_id=ct.candidate_id "
			+ "JOIN technologies t ON t.technology_id=ct.technology_id "
			+ "WHERE (ct.year_exp>=0 AND ct.year_exp <=2)  AND c.date_created > NOW() - INTERVAL '1 YEAR' "
			+ "GROUP BY t.technology_name)t1 ON t1.tech=t.technology_name LEFT JOIN "
			+ "(SELECT t.technology_name AS tech,AVG(c.asking_salary)AS Average FROM candidates c "
			+ "JOIN candidate_technology ct ON c.candidate_id=ct.candidate_id "
			+ "JOIN technologies t ON t.technology_id=ct.technology_id "
			+ "WHERE (ct.year_exp>=0 AND ct.year_exp <=2)  AND (c.date_created < NOW() - INTERVAL '1 YEAR' AND c.date_created >NOW() - INTERVAL '2 YEAR') "
			+ "GROUP BY t.technology_name)t1p ON t1p.tech=t.technology_name LEFT JOIN "
			+ "(SELECT t.technology_name AS tech,AVG(c.asking_salary)as Average FROM candidates c "
			+ "JOIN candidate_technology ct ON c.candidate_id=ct.candidate_id "
			+ "JOIN technologies t ON t.technology_id=ct.technology_id "
			+ "WHERE (ct.year_exp>=2 AND ct.year_exp <=3) AND c.date_created > NOW() - INTERVAL '1 YEAR' "
			+ "GROUP BY t.technology_name)t2 ON t2.tech=t.technology_name LEFT JOIN "
			+ "(SELECT t.technology_name AS tech,AVG(c.asking_salary)as Average FROM candidates c "
			+ "JOIN candidate_technology ct ON c.candidate_id=ct.candidate_id "
			+ "JOIN technologies t ON t.technology_id=ct.technology_id "
			+ "WHERE (ct.year_exp>=2 AND ct.year_exp <=3) AND (c.date_created < NOW() - INTERVAL '1 YEAR' AND c.date_created >NOW() - INTERVAL '2 YEAR') "
			+ "GROUP BY t.technology_name)t2p ON t2p.tech=t.technology_name LEFT JOIN "
			+ "(SELECT t.technology_name AS tech,AVG(c.asking_salary)as Average FROM candidates c "
			+ "JOIN candidate_technology ct ON c.candidate_id=ct.candidate_id "
			+ "JOIN technologies t ON t.technology_id=ct.technology_id "
			+ "WHERE (ct.year_exp>=3 AND ct.year_exp <=4) AND c.date_created > NOW() - INTERVAL '1 YEAR' "
			+ "GROUP BY t.technology_name)t3 ON t3.tech=t.technology_name LEFT JOIN "
			+ "(SELECT t.technology_name AS tech,AVG(c.asking_salary)as Average FROM candidates c "
			+ "JOIN candidate_technology ct ON c.candidate_id=ct.candidate_id "
			+ "JOIN technologies t ON t.technology_id=ct.technology_id "
			+ "WHERE (ct.year_exp>=3 AND ct.year_exp <=4) AND (c.date_created < NOW() - INTERVAL '1 YEAR' AND c.date_created >NOW() - INTERVAL '2 YEAR') "
			+ "GROUP BY t.technology_name)t3p ON t3p.tech=t.technology_name LEFT JOIN "
			+ "(SELECT t.technology_name AS tech,AVG(c.asking_salary)as Average FROM candidates c "
			+ "JOIN candidate_technology ct ON c.candidate_id=ct.candidate_id "
			+ "JOIN technologies t ON t.technology_id=ct.technology_id "
			+ "WHERE ct.year_exp>=5 AND c.date_created > NOW() - INTERVAL '1 YEAR' "
			+ "GROUP BY t.technology_name)t4 ON t4.tech=t.technology_name LEFT JOIN "
			+ "(SELECT t.technology_name AS tech,AVG(c.asking_salary)as Average FROM candidates c "
			+ "JOIN candidate_technology ct ON c.candidate_id=ct.candidate_id "
			+ "JOIN technologies t ON t.technology_id=ct.technology_id "
			+ "WHERE ct.year_exp>=5 AND (c.date_created < NOW() - INTERVAL '1 YEAR' AND c.date_created >NOW() - INTERVAL '2 YEAR') "
			+ "GROUP BY t.technology_name)t4p ON t4p.tech=t.technology_name"
			+ ";", nativeQuery = true)
	List<Object[]> getAskingSalaryVsReport();
	@Query(value = "SELECT t.technology_id AS id,t.technology_name AS tech, "
			+ "(t1.Average-t1P.Average)AS growth02, "
			+ "(t2.Average-t2P.Average)AS growth23, "
			+ "(t3.Average-t3P.Average)AS growth34, "
			+ "(t4.Average-t4P.Average)AS growth5 "
			+ "FROM technologies t LEFT JOIN "
			+ "(SELECT t.technology_name AS tech,AVG(c.asking_salary)AS Average FROM candidates c "
			+ "JOIN candidate_technology ct ON c.candidate_id=ct.candidate_id "
			+ "JOIN technologies t ON t.technology_id=ct.technology_id "
			+ "WHERE (ct.year_exp>=:param1from AND ct.year_exp <=:param1to)  AND c.date_created > NOW() - INTERVAL '1 YEAR' "
			+ "GROUP BY t.technology_name)t1 ON t1.tech=t.technology_name LEFT JOIN "
			+ "(SELECT t.technology_name AS tech,AVG(c.asking_salary)AS Average FROM candidates c "
			+ "JOIN candidate_technology ct ON c.candidate_id=ct.candidate_id "
			+ "JOIN technologies t ON t.technology_id=ct.technology_id "
			+ "WHERE (ct.year_exp>=:param1from AND ct.year_exp <=:param1to)  AND (c.date_created < NOW() - INTERVAL '1 YEAR' AND c.date_created >NOW() - INTERVAL '2 YEAR') "
			+ "GROUP BY t.technology_name)t1p ON t1p.tech=t.technology_name LEFT JOIN "
			+ "(SELECT t.technology_name AS tech,AVG(c.asking_salary)as Average FROM candidates c "
			+ "JOIN candidate_technology ct ON c.candidate_id=ct.candidate_id "
			+ "JOIN technologies t ON t.technology_id=ct.technology_id "
			+ "WHERE (ct.year_exp>=:param2from AND ct.year_exp <=:param2to) AND c.date_created > NOW() - INTERVAL '1 YEAR' "
			+ "GROUP BY t.technology_name)t2 ON t2.tech=t.technology_name LEFT JOIN "
			+ "(SELECT t.technology_name AS tech,AVG(c.asking_salary)as Average FROM candidates c "
			+ "JOIN candidate_technology ct ON c.candidate_id=ct.candidate_id "
			+ "JOIN technologies t ON t.technology_id=ct.technology_id "
			+ "WHERE (ct.year_exp>=:param2from AND ct.year_exp <=:param2to) AND (c.date_created < NOW() - INTERVAL '1 YEAR' AND c.date_created >NOW() - INTERVAL '2 YEAR') "
			+ "GROUP BY t.technology_name)t2p ON t2p.tech=t.technology_name LEFT JOIN "
			+ "(SELECT t.technology_name AS tech,AVG(c.asking_salary)as Average FROM candidates c "
			+ "JOIN candidate_technology ct ON c.candidate_id=ct.candidate_id "
			+ "JOIN technologies t ON t.technology_id=ct.technology_id "
			+ "WHERE (ct.year_exp>=:param3from AND ct.year_exp <=:param3to) AND c.date_created > NOW() - INTERVAL '1 YEAR' "
			+ "GROUP BY t.technology_name)t3 ON t3.tech=t.technology_name LEFT JOIN "
			+ "(SELECT t.technology_name AS tech,AVG(c.asking_salary)as Average FROM candidates c "
			+ "JOIN candidate_technology ct ON c.candidate_id=ct.candidate_id "
			+ "JOIN technologies t ON t.technology_id=ct.technology_id "
			+ "WHERE (ct.year_exp>=:param3from AND ct.year_exp <=:param3to) AND (c.date_created < NOW() - INTERVAL '1 YEAR' AND c.date_created >NOW() - INTERVAL '2 YEAR') "
			+ "GROUP BY t.technology_name)t3p ON t3p.tech=t.technology_name LEFT JOIN "
			+ "(SELECT t.technology_name AS tech,AVG(c.asking_salary)as Average FROM candidates c "
			+ "JOIN candidate_technology ct ON c.candidate_id=ct.candidate_id "
			+ "JOIN technologies t ON t.technology_id=ct.technology_id "
			+ "WHERE ct.year_exp>=:param4atleast AND c.date_created > NOW() - INTERVAL '1 YEAR' "
			+ "GROUP BY t.technology_name)t4 ON t4.tech=t.technology_name LEFT JOIN "
			+ "(SELECT t.technology_name AS tech,AVG(c.asking_salary)as Average FROM candidates c "
			+ "JOIN candidate_technology ct ON c.candidate_id=ct.candidate_id "
			+ "JOIN technologies t ON t.technology_id=ct.technology_id "
			+ "WHERE ct.year_exp>=:param4atleast AND (c.date_created < NOW() - INTERVAL '1 YEAR' AND c.date_created >NOW() - INTERVAL '2 YEAR') "
			+ "GROUP BY t.technology_name)t4p ON t4p.tech=t.technology_name"
			+ ";", nativeQuery = true)
	List<Object[]> testgetAskingSalaryVsReport(@Param("param1from") Integer param1from,@Param("param1to") Integer param1to,
			@Param("param2from") Integer param2from,@Param("param2to") Integer param2to,
			@Param("param3from") Integer param3from,@Param("param3to") Integer param3to,
			@Param("param4atleast") Integer param4atleast
			);
	
	@Query("SELECT c FROM Candidates c WHERE (:filter=:day AND c.dateCreated >:intervalDay) OR "
			+ "(:filter=:month AND c.dateCreated >:intervalMonth) OR (:filter=:year AND c.dateCreated >:intervalYear)")
	public List<AskingSalaryProjection> findAskingSalary(@Param("intervalDay") Date intervalDay,
			@Param("intervalMonth") Date intervalMonth, @Param("intervalYear") Date intervalYear,
			@Param("filter") FilterDate filterEnum, @Param("day") FilterDate day, @Param("year") FilterDate year,
			@Param("month") FilterDate month);

	@Query("SELECT c FROM Candidates c WHERE (:filter=:day AND c.dateCreated >:intervalDay) OR "
			+ "(:filter=:month AND c.dateCreated >:intervalMonth) OR (:filter=:year AND c.dateCreated >:intervalYear)")
	public List<TechnologyProjection> findTechnology(@Param("intervalDay") Date intervalDay,
			@Param("intervalMonth") Date intervalMonth, @Param("intervalYear") Date intervalYear,
			@Param("filter") FilterDate filterEnum, @Param("day") FilterDate day, @Param("year") FilterDate year,
			@Param("month") FilterDate month);

	@Query("SELECT DISTINCT c.candidate_id as candidate_id,CONCAT(c.firstName,' ',c.lastName)as fullName,"
			+ "c.jobTitle as jobTitle, c.totalYearsExp as totalYearsExp, u.firstName as owner,c.dateCreated as dateCreated FROM Candidates c LEFT JOIN c.candidateNotes n LEFT JOIN n.candidateStatus status "
			+ "LEFT JOIN c.userAccount u LEFT JOIN c.candidateSource source "
			+ "LEFT JOIN c.technology t LEFT JOIN t.technology t2")
	Page<SearchResultProjection> findTop50BlankSearch(Pageable pageable);

	@Query(value="(SELECT DISTINCT c.candidate_id as candidate_id,CONCAT(c.first_name,' ',c.last_name)as fullName,"
			+ "c.job_title as jobTitle, c.total_years_exp as totalYearsExp, u.first_name as owner,c.date_created as dateCreated FROM candidates c "
			+ "LEFT JOIN user_account u ON u.user_id=c.user_id  LIMIT 50)",nativeQuery=true)
	Page<SearchResultProjection> blankSearch50(Pageable pageable);
	
	
	
	@Query("SELECT DISTINCT c FROM Candidates c LEFT JOIN c.candidateNotes n LEFT JOIN n.candidateStatus status "
			+ "LEFT JOIN c.userAccount u LEFT JOIN c.candidateSource source "
			+ "LEFT JOIN c.technology t LEFT JOIN t.technology t2 " + "WHERE "
			+ "((cast(:statusIsBest as text) IS NULL) OR status.status LIKE CONCAT('%',cast(:status as text),'%')) "
			+ "AND ((cast(:statusIsExact as text) IS NULL) OR status.status=cast(:status as text)) "
			+ "AND ((cast(:ownerIsBest as text) IS NULL) OR u.firstName LIKE CONCAT('%',cast(:owner as text),'%') OR u.lastName LIKE CONCAT('%',cast(:owner as text),'%')) "
			+ "AND ((cast(:ownerIsExact as text) IS NULL) OR u.firstName=cast(:status as text) OR u.lastName=cast(:status as text)) "
			+ "AND ((cast(:sourceIsBest as text) IS NULL) OR source.sourceName LIKE CONCAT('%',cast(:source as text),'%')) "
			+ "AND ((cast(:sourceIsExact as text) IS NULL) OR source.sourceName=cast(:source as text)) "
			+ "AND ((cast(:nameIsBest as text) IS NULL) OR c.firstName LIKE CONCAT('%',cast(:name as text),'%') OR c.lastName LIKE CONCAT('%',cast(:name as text),'%')) "
			+ "AND ((cast(:nameIsExact as text) IS NULL) OR c.firstName=cast(:name as text) OR c.lastName=cast(:name as text)) "
			+ "AND ((cast(:technologyIsMultiple as text) IS NULL) OR t2.technologyName IN :technology)  "
			+ "AND ((cast(:askingSalaryIsRange as text) IS NULL) OR c.askingSalary >= :askingSalaryMin and c.askingSalary<=:askingSalaryMax ) "
			+ "AND ((cast(:askingSalaryIsMaximum as text) IS NULL) OR c.askingSalary <=:askingSalary ) "
			+ "AND ((cast(:askingSalaryIsAtleast as text) IS NULL) OR c.askingSalary >=:askingSalary ) "
			+ "AND ((cast(:askingSalaryIsExact as text) IS NULL) OR c.askingSalary=:askingSalary) ")

	Page<SearchResultProjection> adv(@Param("statusIsBest") String statusIsBest,
			@Param("statusIsExact") String statusIsExact, @Param("status") String status,
			@Param("ownerIsBest") String ownerIsBest, @Param("ownerIsExact") String ownerIsExact,
			@Param("owner") String owner, @Param("sourceIsBest") String sourceIsBest,
			@Param("sourceIsExact") String sourceIsExact, @Param("source") String source,
			@Param("nameIsBest") String nameIsBest, @Param("nameIsExact") String nameIsExact,
			@Param("name") String name, @Param("technologyIsMultiple") String technologyIsMultiple,
			@Param("technology") List<String> technology, @Param("askingSalaryIsRange") String askingSalaryIsRange,
			@Param("askingSalaryMin") Double askingSalaryMin, @Param("askingSalaryMax") Double askingSalaryMax,
			@Param("askingSalaryIsMaximum") String askingSalaryIsMaximum,
			@Param("askingSalaryIsAtleast") String askingSalaryIsAtleast,
			@Param("askingSalaryIsExact") String askingSalaryIsExact, @Param("askingSalary") Double askingSalary,
			Pageable pageable);

//	@Query("SELECT DISTINCT c FROM Candidates c LEFT JOIN c.candidateNotes n LEFT JOIN n.candidateStatus status "
//			+ "LEFT JOIN c.userAccount u LEFT JOIN c.candidateSource source "
//			+ "LEFT JOIN c.technology t LEFT JOIN t.technology t2 " + "WHERE "
//			+ "(:statusType IS NULL OR ((:statusType=:best AND LOWER(status.status) LIKE CONCAT('%',:status,'%')) "
//			+ "OR (:statusType=:exact AND LOWER(status.status) = :status))) "
//			+ "AND(:nameType IS NULL OR((:nameType=:best AND (LOWER(c.firstName) LIKE CONCAT('%',:name,'%') OR LOWER(c.lastName) LIKE CONCAT('%',:name,'%'))) "
//			+ "OR (:nameType=:exact AND LOWER(c.firstName) = :name)) ) "
//			+ "AND(:askingsalaryType IS NULL OR((:askingsalaryType=:exact AND c.askingSalary =:askingsalary) "
//			+ "OR (:askingsalaryType=:atleast AND c.askingSalary >= :askingsalary) "
//			+ "OR (:askingsalaryType=:maximum AND c.askingSalary <= :askingsalary) "
//			+ "OR (:askingsalaryType=:range AND (c.askingSalary >= :min AND c.askingSalary<=:max))) ) "
//			+ "AND(:ownerType IS NULL OR((:ownerType=:exact AND (LOWER(u.firstName) =:owner OR LOWER(u.lastName)=:owner)) "
//			+ "OR (:ownerType=:best AND (LOWER(u.firstName) LIKE CONCAT('%',:owner,'%') OR LOWER(u.lastName) LIKE CONCAT('%',:owner,'%')))) ) "
//			+ "AND(:sourceType IS NULL OR((:sourceType=:exact AND (LOWER(source.sourceName) =:source)) "
//			+ "OR (:sourceType=:best AND (LOWER(source.sourceName) LIKE CONCAT('%',:source,'%')))) ) "
//			+ "AND(:technologyType IS NULL OR((:technologyType=:multiple AND t2.technologyName IN :technology)) ) "
//			+ "AND(:jobTitleType IS NULL OR((:jobTitleType=:multiple AND c.jobTitle IN :jobTitle)) )"
//			
//			)
	@Query("SELECT DISTINCT c.candidate_id as candidate_id,CONCAT(c.firstName,' ',c.lastName)as fullName,"
			+ "c.jobTitle as jobTitle, c.totalYearsExp as totalYearsExp, u.firstName as owner,c.dateCreated as dateCreated FROM Candidates c LEFT JOIN c.candidateNotes n LEFT JOIN n.candidateStatus status "
			+ "LEFT JOIN c.userAccount u LEFT JOIN c.candidateSource source "
			+ "LEFT JOIN c.technology t LEFT JOIN t.technology t2 " + "WHERE "
			+ "(:statusType IS NULL OR ((:statusType=:best AND LOWER(status.status) LIKE CONCAT('%',:status,'%')) "
			+ "OR (:statusType=:exact AND LOWER(status.status) = :status))) "
			+ "AND(:nameType IS NULL OR((:nameType=:best AND (LOWER(c.firstName) LIKE CONCAT('%',:name,'%') OR LOWER(c.lastName) LIKE CONCAT('%',:name,'%'))) "
			+ "OR (:nameType=:exact AND LOWER(c.firstName) = :name)) ) "
			+ "AND(:askingsalaryType IS NULL OR((:askingsalaryType=:exact AND c.askingSalary =:askingsalary) "
			+ "OR (:askingsalaryType=:atleast AND c.askingSalary >= :askingsalary) "
			+ "OR (:askingsalaryType=:maximum AND c.askingSalary <= :askingsalary) "
			+ "OR (:askingsalaryType=:range AND (c.askingSalary >= :min AND c.askingSalary<=:max))) ) "
			+ "AND(:ownerType IS NULL OR((:ownerType=:exact AND (LOWER(u.firstName) =:owner OR LOWER(u.lastName)=:owner)) "
			+ "OR (:ownerType=:best AND (LOWER(u.firstName) LIKE CONCAT('%',:owner,'%') OR LOWER(u.lastName) LIKE CONCAT('%',:owner,'%')))) ) "
			+ "AND(:sourceType IS NULL OR((:sourceType=:exact AND (LOWER(source.sourceName) =:source)) "
			+ "OR (:sourceType=:best AND (LOWER(source.sourceName) LIKE CONCAT('%',:source,'%')))) ) "
			+ "AND(:technologyType IS NULL OR((:technologyType=:multiple AND LOWER(t2.technologyName) IN :technology)) "
			+ "OR (:technologyType=:best AND (LOWER(t2.technologyName) LIKE CONCAT('%',:technology,'%')) )) "
			+ "AND(:jobTitleType IS NULL OR((:jobTitleType=:multiple AND LOWER(c.jobTitle) IN :jobTitle)) "
			+ "OR(:jobTitleType =:best AND (LOWER(c.jobTitle) LIKE CONCAT('%',:jobTitle,'%'))))"
			
			)

	Page<SearchResultProjection> chk(@Param("statusType") TypeOfSearch statusType, @Param("status") String status,
			@Param("nameType") TypeOfSearch nameType, @Param("name") String name,
			@Param("askingsalaryType") TypeOfSearch askingsalaryType, @Param("askingsalary") Double askingsalary,
			@Param("min") Double min, @Param("max") Double max, @Param("ownerType") TypeOfSearch ownerType,
			@Param("owner") String owner, @Param("sourceType") TypeOfSearch sourceType, @Param("source") String source,
			@Param("technologyType") TypeOfSearch technologyType, @Param("technology") List<String> technology,
			@Param("jobTitleType") TypeOfSearch jobTitlesType, @Param("jobTitle") List<String> jobTitle,
			@Param("atleast") TypeOfSearch atleast, @Param("best") TypeOfSearch best,
			@Param("exact") TypeOfSearch exact, @Param("maximum") TypeOfSearch maximum,
			@Param("multiple") TypeOfSearch multiple, @Param("range") TypeOfSearch range, Pageable page

	);

	@Query("SELECT c FROM Candidates c WHERE c.candidate_id=:id")
	Candidates getById(Long id);

	@Query(value="SELECT DISTINCT job_title FROM candidates",nativeQuery = true)
	List<String> getJobTitles();

	@Query("SELECT DISTINCT c.candidate_id as candidate_id,CONCAT(c.firstName,' ',c.lastName)as fullName,"
			+ "c.jobTitle as jobTitle, c.totalYearsExp as totalYearsExp, u.firstName as owner, c.dateCreated as dateCreated FROM Candidates c LEFT JOIN c.candidateNotes n LEFT JOIN n.candidateStatus status "
			+ "LEFT JOIN c.userAccount u LEFT JOIN c.candidateSource source "
			+ "LEFT JOIN c.technology t LEFT JOIN t.technology t2 "
			+ "WHERE u.user_id=:recruiterId "
			+ "AND(:currentWeek IS FALSE OR c.dateCreated >=DATE_TRUNC('week',CURRENT_DATE)) "
			)
	Page<SearchResultProjection> getByRecruiterId(Pageable page,@Param("recruiterId")Long recruiterId,@Param("currentWeek") boolean currentWeek);

}
