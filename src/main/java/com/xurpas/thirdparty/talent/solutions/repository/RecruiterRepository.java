package com.xurpas.thirdparty.talent.solutions.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import com.xurpas.thirdparty.talent.solutions.model.User;
import com.xurpas.thirdparty.talent.solutions.payload.RecruiterTestProjection;

@Repository
public interface RecruiterRepository extends JpaRepository<User, Integer> {
	public List<User> findByFirstName(String firstName);

	@Query(value = "SELECT DISTINCT a.user_id, a.first_name,COUNT(d) AS total_history,a.last_login_date,"
			+ "(SELECT DISTINCT COUNT(innercan) FROM user_account inneruser "
			+ "INNER JOIN candidates innercan ON inneruser.user_id=innercan.user_id "
			+ "WHERE innercan.date_created >=DATE_TRUNC('week',CURRENT_DATE) AND a.user_id=inneruser.user_id "
			+ "GROUP BY inneruser.user_id)   FROM user_account a "
			+ "INNER JOIN candidates d ON a.user_id=d.user_id" 
			+ "	   GROUP BY a.user_id", nativeQuery = true)
	List<Object[]> getSummary();

	@Query(value = "SELECT DISTINCT a.user_id, CONCAT(a.first_name,' ',a.last_name),COUNT(d) AS total_history,a.last_login_date,"
			+ "(SELECT DISTINCT COUNT(innercan) FROM user_account inneruser "
			+ "INNER JOIN candidates innercan ON inneruser.user_id=innercan.user_id "
			+ "WHERE innercan.date_created >=DATE_TRUNC('week',CURRENT_DATE) AND a.user_id=inneruser.user_id "
			+ "GROUP BY inneruser.user_id)   FROM user_account a "
			+ "INNER JOIN candidates d ON a.user_id=d.user_id " 
			+ "WHERE LOWER(a.first_name) LIKE CONCAT('%',LOWER(:user),'%') OR LOWER(a.last_name) LIKE CONCAT('%',LOWER(:user),'%') "
			+ "GROUP BY a.user_id", nativeQuery = true)
	List<Object[]> searchRecruiter(@Param("user") String user);

	@Query( value="SELECT DISTINCT(u.user_id) as user_id,u.last_login_date as last_login_date,"
			+ "COUNT(c) as total_entries_historical , u.firstName as name, "
			+ "COALESCE((SELECT DISTINCT COALESCE(COUNT(iu),0)  FROM Candidates ic "
			+ "RIGHT JOIN ic.userAccount iu "
			+ "WHERE ic.dateCreated >=DATE_TRUNC('week',CURRENT_DATE) AND u.user_id=iu.user_id "
			+ "GROUP BY iu.user_id),0) as total_entries_this_week  FROM Candidates c "
			+ "RIGHT JOIN c.userAccount u "
			+ "GROUP BY u.user_id",
			
			countQuery = "SELECT COUNT(u.user_id) FROM Candidates c "
					+ "RIGHT JOIN c.userAccount u "
					+ "GROUP BY u.user_id"
					)
	Page<RecruiterTestProjection> getRecruiters(Pageable page);
	
	
	@Query( value="SELECT DISTINCT(u.user_id) as user_id,u.last_login_date as last_login_date,"
			+ "COUNT(c) as total_entries_historical, u.firstName as name, "
			+ "COALESCE( (SELECT DISTINCT COALESCE(COUNT(iu),0) FROM Candidates ic "
			+ "RIGHT JOIN ic.userAccount iu "
			+ "WHERE ic.dateCreated >=DATE_TRUNC('week',CURRENT_DATE) AND u.user_id=iu.user_id "
			+ "GROUP BY iu.user_id),0) as  total_entries_this_week FROM Candidates c "
			+ "RIGHT JOIN c.userAccount u "
			+ "WHERE LOWER(u.firstName) LIKE CONCAT('%',LOWER(:user),'%') OR LOWER(u.lastName) LIKE CONCAT('%',LOWER(:user),'%') "
			+ "GROUP BY u.user_id",
			
			countQuery = "SELECT COUNT(u.user_id) FROM Candidates c "
					+ "RIGHT JOIN c.userAccount u "
					+ "WHERE LOWER(u.firstName) LIKE CONCAT('%',LOWER(:user),'%') OR LOWER(u.lastName) LIKE CONCAT('%',LOWER(:user),'%') "					
					+ "GROUP BY u.user_id"
					)
	Page<RecruiterTestProjection> getRecruiterByName(@Param("user") String user,Pageable page);
	@Query( value="SELECT DISTINCT u.user_id as user_id,u.last_login_date as last_login_date,"
			+ "COUNT(c) as total_entries_historical, u.firstName as name, "
			+ "(SELECT DISTINCT COUNT(iu)as total_entries_this_week FROM Candidates ic "
			+ "RIGHT JOIN ic.userAccount iu "
			+ "WHERE ic.dateCreated >=DATE_TRUNC('week',CURRENT_DATE) AND u.user_id=iu.user_id "
			+ "GROUP BY iu.user_id) as total_entries_this_week FROM Candidates c "
			+ "RIGHT JOIN c.userAccount u "
			+ "GROUP BY u.user_id",
			countQuery = "SELECT DISTINCT COUNT(u.user_id) FROM Candidates c "
					+ "RIGHT JOIN c.userAccount u "
					+ "GROUP BY u.user_id"
			)
	Page<RecruiterTestProjection> testrec(Pageable page );
	
//	@Query("SELECT DISTINCT a.user_id, CONCAT(a.first_name,' ',a.last_name),COUNT(d) AS total_history,a.last_login_date,"
//			+ "(SELECT DISTINCT COUNT(innercan) FROM user_account inneruser "
//			+ "INNER JOIN candidates innercan ON inneruser.user_id=innercan.user_id "
//			+ "WHERE innercan.date_created >=DATE_TRUNC('week',CURRENT_DATE) AND a.user_id=inneruser.user_id "
//			+ "GROUP BY inneruser.user_id)   FROM user_account a "
//			+ "INNER JOIN candidates d ON a.user_id=d.user_id " 
//			+ "WHERE LOWER(a.first_name) LIKE CONCAT('%',LOWER(:user),'%') OR LOWER(a.last_name) LIKE CONCAT('%',LOWER(:user),'%') "
//			+ "GROUP BY a.user_id")
	@Query(value = "select count(c)as total,"
			+ "(select count(ic)as week from candidates ic where ic.date_created >= DATE_TRUNC('week',CURRENT_DATE)) "
			+ "from candidates c;", nativeQuery = true)
	List<Object[]> getTotal();
}
