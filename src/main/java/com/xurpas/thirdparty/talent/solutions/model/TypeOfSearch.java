package com.xurpas.thirdparty.talent.solutions.model;

public enum TypeOfSearch {
	EXACT_MATCH, BEST_MATCH, ATLEAST, MAXIMUM, RANGE, MULTIPLE
}
