package com.xurpas.thirdparty.talent.solutions.specs;

import org.springframework.data.jpa.domain.Specification;

import com.xurpas.thirdparty.talent.solutions.payload.SearchResultProjection;

public class SearchResultProjectionSpecs {

	public static Specification<SearchResultProjection> getSpecs(String status, String name, String owner,
			double askingSalary, String technologies, String source) {
		
		
		Specification<SearchResultProjection> spec=null;
		Specification<SearchResultProjection> temp=null;
		
		if(!status.isEmpty()&&status!=null) {
			spec=getSearchResultProjectionByStatus(status);
			temp=spec!=null?Specification.where(spec).and(temp):temp;
		}
		if(!name.isEmpty()&&name!=null) {
			spec=getSearchResultProjectionByName(name);
			temp=spec!=null?Specification.where(spec).and(temp):temp;
		}
		if(!owner.isEmpty()&&owner!=null) {
			spec=getSearchResultProjectionByOwner(owner);
			temp=spec!=null?Specification.where(spec).and(temp):temp;
		}
		if(askingSalary!=0) {
			spec=getSearchResultProjectionByAskingSalary(askingSalary);
			temp=spec!=null?Specification.where(spec).and(temp):temp;
		}
		if(!technologies.isEmpty()&&technologies!=null) {
			spec=getSearchResultProjectionByTechnologies(technologies);
			temp=spec!=null?Specification.where(spec).and(temp):temp;
		}
		if(!source.isEmpty()&&source!=null) {
			spec=getSearchResultProjectionBySource(source);
			temp=spec!=null?Specification.where(spec).and(temp):temp;
		}
		return null;
	
	}

	private static Specification<SearchResultProjection> getSearchResultProjectionBySource(String source) {
		return(root,query,criteriaBuilder)->{
			return criteriaBuilder.like(criteriaBuilder.lower(root.get("source_name")),"%"+source.toLowerCase()+"%");
		};
	}

	private static Specification<SearchResultProjection> getSearchResultProjectionByTechnologies(String technologies) {
		return(root,query,criteriaBuilder)->{
			return criteriaBuilder.like(criteriaBuilder.lower(root.get("technology_name")),"%"+technologies.toLowerCase()+"%");
		};
	}

	private static Specification<SearchResultProjection> getSearchResultProjectionByAskingSalary(double askingSalary) {
		return(root,query,criteriaBuilder)->{
			return criteriaBuilder.like(criteriaBuilder.lower(root.get("askingSalary")),"%"+askingSalary+"%");
		};
	}

	private static Specification<SearchResultProjection> getSearchResultProjectionByOwner(String owner) {
		return(root,query,criteriaBuilder)->{
			return criteriaBuilder.like(criteriaBuilder.lower(root.get("firstName")),"%"+owner.toLowerCase()+"%");
		};
	}

	private static Specification<SearchResultProjection> getSearchResultProjectionByName(String name) {
		return(root,query,criteriaBuilder)->{
			return criteriaBuilder.like(criteriaBuilder.lower(root.get("firstName")),"%"+name.toLowerCase()+"%");
		};
	}

	private static Specification<SearchResultProjection> getSearchResultProjectionByStatus(String status) {
		return(root,query,criteriaBuilder)->{
			return criteriaBuilder.like(criteriaBuilder.lower(root.get("status")),"%"+status.toLowerCase()+"%");
		};
		
	}
	
	

}
