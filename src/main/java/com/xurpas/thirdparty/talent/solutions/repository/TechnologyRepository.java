package com.xurpas.thirdparty.talent.solutions.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xurpas.thirdparty.talent.solutions.model.Technology;

@Repository
public interface TechnologyRepository extends JpaRepository<Technology, Integer>{

	@Query("SELECT t FROM Technology t WHERE t.technology_id=:id")
	Technology getTechnologyById(@Param("id")Integer id);
	
	
	boolean existsById(Integer id);
}
