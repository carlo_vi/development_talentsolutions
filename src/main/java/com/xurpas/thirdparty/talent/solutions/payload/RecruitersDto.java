package com.xurpas.thirdparty.talent.solutions.payload;

import java.util.List;

import com.xurpas.thirdparty.talent.solutions.model.User;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RecruitersDto {
	private List<User> users;
	private long totalElements;
	private long totalPages;
	private int currentPage;
	public RecruitersDto(List<User> users) {
		this.users = users;
	}
	

	
	
}
