package com.xurpas.thirdparty.talent.solutions.payload;

import java.util.List;

import org.springframework.data.domain.Page;

import com.xurpas.thirdparty.talent.solutions.model.Candidates;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SearchPaginationDto {
	List<SearchResultProjection> candidates;
	private long totalElements;
	private long totalPages;
	private int currentPage;
}
