package com.xurpas.thirdparty.talent.solutions.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "candidate_source")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CandidateSource {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer sourceId;
	
	@Column(name = "source_name", length = 50)
	private String sourceName;

	public CandidateSource(Integer sourceId) {
		this.sourceId = sourceId;
	}
	
	
}
