package com.xurpas.thirdparty.talent.solutions.payload;

import java.math.BigInteger;

import lombok.Data;

@Data
public class SourceVsDto {
private String source_name;
private BigInteger current_year;
private BigInteger previous_year;
private BigInteger growth;
}
