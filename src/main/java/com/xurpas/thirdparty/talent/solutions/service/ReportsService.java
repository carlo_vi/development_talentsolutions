package com.xurpas.thirdparty.talent.solutions.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.xurpas.thirdparty.talent.solutions.model.CandidateTechnology;
import com.xurpas.thirdparty.talent.solutions.model.Candidates;
import com.xurpas.thirdparty.talent.solutions.model.Config;
import com.xurpas.thirdparty.talent.solutions.payload.AskingSalaryDto;
import com.xurpas.thirdparty.talent.solutions.payload.AskingSalaryVsDto;
import com.xurpas.thirdparty.talent.solutions.payload.ReportFilter;
import com.xurpas.thirdparty.talent.solutions.payload.ReportFilterDto;
import com.xurpas.thirdparty.talent.solutions.payload.SourceVsDto;
import com.xurpas.thirdparty.talent.solutions.payload.SourceDto;
import com.xurpas.thirdparty.talent.solutions.payload.TechnologyDto;
import com.xurpas.thirdparty.talent.solutions.payload.TechnologyVsDto;
import com.xurpas.thirdparty.talent.solutions.repository.CandidateRepository;
import com.xurpas.thirdparty.talent.solutions.repository.CandidateTechnologyRepository;
import com.xurpas.thirdparty.talent.solutions.repository.ConfigRepository;

@Service
public class ReportsService {

	@Autowired
	private CandidateTechnologyRepository repo;

	@Autowired
	private CandidateRepository candidateRepo;
	@Autowired
	private ConfigRepository configRepo;
//	@Autowired
//	private ConstantValuesConfig constantValuesConfig;
	public List<CandidateTechnology> listAllTechnology() {

		return repo.findAll();

	}

	public List<?> getSourceReport(ReportFilterDto reportFilterDto) {

		List<SourceVsDto> sourceVs = new ArrayList<>();
		List<SourceDto> sourceYtd = new ArrayList<>();

		if (reportFilterDto.getReportFilter() == ReportFilter.lastYearVsYTD) {

			for (Object[] obj : candidateRepo.getSourceVsReport()) {
				SourceVsDto test = new SourceVsDto();
				test.setSource_name(String.valueOf(obj[0]));
				test.setCurrent_year((BigInteger) (obj[1]));
				test.setPrevious_year((BigInteger) (obj[2]));
				test.setGrowth((BigInteger) (obj[3]));
				sourceVs.add(test);
			}
			return sourceVs;
		} else {
			for (Object[] obj : candidateRepo.getSourceReport(ReportFilter.YTD, reportFilterDto.getReportFilter())) {
				SourceDto test = new SourceDto();
				test.setCount((BigInteger) (obj[1]));
				test.setSourceName(String.valueOf(obj[0]));
				sourceYtd.add(test);
			}
			return sourceYtd;
		}
	}

	public List<?> getTechnologyReport(ReportFilterDto reportFilterDto) {

		List<TechnologyDto> technologyYtd = new ArrayList<>();
		List<TechnologyVsDto> technologyVs = new ArrayList<>();

		if (reportFilterDto.getReportFilter() == ReportFilter.lastYearVsYTD) {
			for (Object[] obj : candidateRepo.getTechnologyVsReport()) {
				TechnologyVsDto test = new TechnologyVsDto();
				test.setTechnology_name(String.valueOf(obj[0]));
				test.setPrevious_year((BigInteger) (obj[1]));
				test.setCurrent_year((BigInteger) (obj[2]));
				test.setGrowth((BigInteger) (obj[3]));
				technologyVs.add(test);
			}
			return technologyVs;
		} else {
			for (Object[] obj : candidateRepo.getTechnologyReport(ReportFilter.YTD,
					reportFilterDto.getReportFilter())) {
				TechnologyDto test = new TechnologyDto();
				test.setCount((BigInteger) (obj[1]));
				test.setTechnologyName(String.valueOf(obj[0]));
				technologyYtd.add(test);
			}
			return technologyYtd;
		}

	}

	public List<?> testgetAskingSalaryReport(ReportFilterDto reportFilterDto) {

		List<AskingSalaryDto> askingSalaryYtd = new ArrayList<>();

		List<AskingSalaryVsDto> askingSalaryVs = new ArrayList<>();
		if (reportFilterDto.getReportFilter() == ReportFilter.lastYearVsYTD) {
			
			for (Object[] obj : candidateRepo.getAskingSalaryVsReport()) {
				AskingSalaryVsDto test = new AskingSalaryVsDto();
				test.setId((Integer) (obj[0]));
				test.setTechnology_name(String.valueOf(obj[1]));
				test.setGrowth02((Double) (obj[2]));
				test.setGrowth23((Double) (obj[3]));
				test.setGrowth34((Double) (obj[4]));
				test.setGrowth5((Double) (obj[5]));

				askingSalaryVs.add(test);
			}
			return askingSalaryVs;
		} else {
			for (Object[] obj : candidateRepo.getAskingSalaryReport(ReportFilter.YTD,
					reportFilterDto.getReportFilter())) {
				AskingSalaryDto test = new AskingSalaryDto();
				test.setTechId((Integer) (obj[0]));
				test.setTechName(String.valueOf(obj[1]));
				test.setN1((BigInteger) (obj[2]));
				test.setValue1((Double) (obj[3]));
				test.setN2((BigInteger) (obj[4]));
				test.setValue2((Double) (obj[5]));
				test.setN3((BigInteger) (obj[6]));
				test.setValue3((Double) (obj[7]));
				test.setN4((BigInteger) (obj[8]));
				test.setValue4((Double) (obj[9]));

				askingSalaryYtd.add(test);
			}
			return askingSalaryYtd;
		}

	}
	public List<?> getAskingSalaryReport(ReportFilterDto reportFilterDto) {

		List<AskingSalaryDto> askingSalaryYtd = new ArrayList<>();

		List<AskingSalaryVsDto> askingSalaryVs = new ArrayList<>();
		int param1From = Integer.parseInt(configRepo.findByName("AS_YTDVsLY_1_from"));
		int param1to = Integer.parseInt(configRepo.findByName("AS_YTDVsLY_1_to"));
		int param2From = Integer.parseInt(configRepo.findByName("AS_YTDVsLY_2_from"));
		int param2to = Integer.parseInt(configRepo.findByName("AS_YTDVsLY_2_to"));
		int param3From = Integer.parseInt(configRepo.findByName("AS_YTDVsLY_3_from"));
		int param3to = Integer.parseInt(configRepo.findByName("AS_YTDVsLY_3_to"));
		int param4Atleast = Integer.parseInt(configRepo.findByName("AS_YTDVsLY_4_atleast"));
		if (reportFilterDto.getReportFilter() == ReportFilter.lastYearVsYTD) {
			
			
		
		
			for (Object[] obj : candidateRepo.testgetAskingSalaryVsReport(param1From,param1to,
					param2From,param2to,
					param3From,param3to,
					param4Atleast)) {
				AskingSalaryVsDto test = new AskingSalaryVsDto();
				test.setId((Integer) (obj[0]));
				test.setTechnology_name(String.valueOf(obj[1]));
				test.setGrowth02((Double) (obj[2]));
				test.setGrowth23((Double) (obj[3]));
				test.setGrowth34((Double) (obj[4]));
				test.setGrowth5((Double) (obj[5]));

				askingSalaryVs.add(test);
			}
			return askingSalaryVs;
		} else {
			for (Object[] obj : candidateRepo.testgetAskingSalaryReport(ReportFilter.YTD,
					reportFilterDto.getReportFilter(),param1From,param1to,
					param2From,param2to,
					param3From,param3to,
					param4Atleast)) {
				AskingSalaryDto test = new AskingSalaryDto();
				test.setTechId((Integer) (obj[0]));
				test.setTechName(String.valueOf(obj[1]));
				test.setN1((BigInteger) (obj[2]));
				test.setValue1((Double) (obj[3]));
				test.setN2((BigInteger) (obj[4]));
				test.setValue2((Double) (obj[5]));
				test.setN3((BigInteger) (obj[6]));
				test.setValue3((Double) (obj[7]));
				test.setN4((BigInteger) (obj[8]));
				test.setValue4((Double) (obj[9]));

				askingSalaryYtd.add(test);
			}
			return askingSalaryYtd;
		}

	}

	/*
	 * public Optional<Candidates> listAllAskingSalary() {
	 * 
	 * return candidateRepo.findCandidateSourcebyPast60days(); }
	 */
}
