package com.xurpas.thirdparty.talent.solutions.payload;

import java.math.BigInteger;

import lombok.Data;

@Data
public class TechnologyVsDto {
private String technology_name;
private BigInteger current_year;
private BigInteger previous_year;
private BigInteger growth;
}
