package com.xurpas.thirdparty.talent.solutions.payload;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.xurpas.thirdparty.talent.solutions.repository.CandidateRepository;

public interface AskingSalaryProjection {
	
    //Getting Asking Salary
    String getcandidate_id();
    String getAskingSalary();
}
