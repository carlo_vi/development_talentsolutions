package com.xurpas.thirdparty.talent.solutions.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.xurpas.thirdparty.talent.solutions.model.Config;

public interface ConfigRepository extends JpaRepository<Config, Integer>{

	Config getByName(String name);
	
	@Query(value="SELECT value FROM config where name=:name",nativeQuery = true)
	String findByName(@Param("name") String name);
}
