package com.xurpas.thirdparty.talent.solutions.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.xurpas.thirdparty.talent.solutions.model.CandidateTechnology;
import com.xurpas.thirdparty.talent.solutions.payload.SourcesProjection;

@Repository
public interface CandidateTechnologyRepository extends JpaRepository<CandidateTechnology, Integer> {

	@Query(value = "SELECT ct.id,ct.candidate_id,ct.technology_id,tech.technology_name,"
			+ "candidates.date_created,ct.year_exp FROM candidate_technology AS ct "
			+ "LEFT JOIN technologies AS tech ON ct.technology_id = tech.technology_id "
			+ "LEFT JOIN candidates AS candidates ON ct.candidate_id = candidates.candidate_id "
			+ "WHERE date_created > now() - interval '60' day", nativeQuery = true)
	public List<CandidateTechnology> findCandidateTechnologybyPast60days();

	@Query(value = "SELECT ct.id,ct.candidate_id,ct.technology_id,tech.technology_name,"
			+ "candidates.date_created,ct.year_exp FROM candidate_technology AS ct "
			+ "LEFT JOIN technologies AS tech ON ct.technology_id = tech.technology_id "
			+ "LEFT JOIN candidates AS candidates ON ct.candidate_id = candidates.candidate_id "
			+ "WHERE date_created > now() - interval '1' month", nativeQuery = true)
	public List<CandidateTechnology> findCandidateTechnologybyMTD();

	@Query(value = "SELECT ct.id,ct.candidate_id,ct.technology_id,tech.technology_name,"
			+ "candidates.date_created,ct.year_exp FROM candidate_technology AS ct "
			+ "LEFT JOIN technologies AS tech ON ct.technology_id = tech.technology_id "
			+ "LEFT JOIN candidates AS candidates ON ct.candidate_id = candidates.candidate_id "
			+ "WHERE date_created > now() - interval '1' year", nativeQuery = true)
	public List<CandidateTechnology> findCandidateTechnologybyYTD();
	
	

	@Query("SELECT t FROM CandidateTechnology t WHERE t.id=:id")
	public CandidateTechnology findCandidateTechnologyById(@Param("id") Integer id);

	@Query(value = "SELECT * FROM candidate_technology t JOIN candidates c ON t.candidate_id=c.candidate_id WHERE c.candidate_id=:candidateId", nativeQuery = true)
	public List<CandidateTechnology> getCandidateTechnologyByCandidateId(@Param("candidateId") Long candidateId);
}
