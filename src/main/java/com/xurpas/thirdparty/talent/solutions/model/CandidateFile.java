package com.xurpas.thirdparty.talent.solutions.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "candidate_file")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class CandidateFile {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String folderId;

	private String link;
	@Column(unique = true)
	private String fileId;

	@ManyToOne(fetch = FetchType.EAGER)
	private FileType fileType;
	@Transient
	private String path;
	public CandidateFile(String folderId, String fileId, FileType fileType) {
		this.folderId = folderId;
		this.fileId = fileId;
		this.fileType = fileType;
	}

}
