package com.xurpas.thirdparty.talent.solutions.model;

import lombok.Getter;


import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Table(name = "roles")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer roleId;

    @Column(length = 20)
    private String roleName;
    
    public Role() { }
    
    
    public Role(String roleName) {
        this.roleName = roleName;
    }
     
    public Role(Integer roleId, String roleName) {
        this.roleId = roleId;
        this.roleName = roleName;
    }
 
    public Role(Integer roleId) {
        this.roleId = roleId;
    }
    
    
    
 
    public Integer getRoleId() {
		return roleId;
	}


	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}


	public String getRoleName() {
		return roleName;
	}


	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}


	@Override
    public String toString() {
        return this.roleName;
    }
    
	

	
    
}