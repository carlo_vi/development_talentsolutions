package com.xurpas.thirdparty.talent.solutions.repository;




import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Repository;

import com.xurpas.thirdparty.talent.solutions.model.User;

import java.util.Optional;

//This will access the database
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	Optional<User> findByEmail(String email);
	User findUserByEmail(String email);
	
	@Query(value="SELECT CONCAT(c.first_name,' ',c.last_name) FROM user_account c WHERE c.email=:email",nativeQuery =true)
	String getUserFullNameByEmail(@Param("email")String email);
    //Optional<User> findByUsernameOrEmail(String username, String email);
  //  Optional<User> findByUsername(String username);
   // Boolean existsByUsername(String username);
	 
	Boolean existsByEmail(String email);
   
	
	
	
	
}