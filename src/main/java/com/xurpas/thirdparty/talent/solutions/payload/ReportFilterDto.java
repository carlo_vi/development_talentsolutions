package com.xurpas.thirdparty.talent.solutions.payload;

import lombok.Data;

@Data
public class ReportFilterDto {
	ReportFilter reportFilter;
}
