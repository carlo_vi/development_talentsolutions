package com.xurpas.thirdparty.talent.solutions.resource;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.websocket.server.PathParam;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.xurpas.thirdparty.talent.solutions.exception.ResponseHandler;
import com.xurpas.thirdparty.talent.solutions.model.CandidateFile;
import com.xurpas.thirdparty.talent.solutions.model.CandidateSource;
import com.xurpas.thirdparty.talent.solutions.model.CandidateStatus;
import com.xurpas.thirdparty.talent.solutions.model.CandidateTechnology;
import com.xurpas.thirdparty.talent.solutions.model.Candidates;
import com.xurpas.thirdparty.talent.solutions.model.FileItem;
import com.xurpas.thirdparty.talent.solutions.model.FileType;
import com.xurpas.thirdparty.talent.solutions.model.Technology;
import com.xurpas.thirdparty.talent.solutions.payload.AddCandidateDto;
import com.xurpas.thirdparty.talent.solutions.payload.CVDto;
import com.xurpas.thirdparty.talent.solutions.payload.GetCandidateDto;
import com.xurpas.thirdparty.talent.solutions.payload.GetCandidateProjection;
import com.xurpas.thirdparty.talent.solutions.payload.LoadOptionsDto;
import com.xurpas.thirdparty.talent.solutions.payload.UpdateCandidateDto;
import com.xurpas.thirdparty.talent.solutions.payload.AddCandidateDto.FilesDto;
import com.xurpas.thirdparty.talent.solutions.payload.AddCandidateDto.TechnologiesDto;
import com.xurpas.thirdparty.talent.solutions.repository.CandidateFileRepository;
import com.xurpas.thirdparty.talent.solutions.repository.CandidateRepository;
import com.xurpas.thirdparty.talent.solutions.repository.CandidateSourceRepository;
import com.xurpas.thirdparty.talent.solutions.repository.CandidateStatusRepository;
import com.xurpas.thirdparty.talent.solutions.repository.FileTypeRepository;
import com.xurpas.thirdparty.talent.solutions.repository.TechnologyRepository;
import com.xurpas.thirdparty.talent.solutions.service.CandidateService;

import ch.qos.logback.core.joran.conditional.IfAction;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeRequestUrl;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.Value;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.Permissions.Get;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.About;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.Permission;
import com.google.api.services.drive.model.PermissionList;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/candidates")

public class CandidateResource {

	@Autowired
	CandidateService candidateService;
	@Autowired
	CandidateFileRepository fileRepo;
	@Autowired
	FileTypeRepository fileTypeRepo;

	@Autowired
	CandidateRepository candidateRepo;
	@Autowired
	CandidateSourceRepository sourceRepo;
	@Autowired
	TechnologyRepository technologyRepo;
	@Autowired
	CandidateStatusRepository statusRepo;

	private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
	/** Global instance of the JSON factory. */
	private static final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();

	/**
	 * Global instance of the scopes required by this quickstart. If modifying these
	 * scopes, delete your previously saved tokens/ folder.
	 */
	private static final List<String> SCOPES = Collections.singletonList(DriveScopes.DRIVE);

	private String CALLBACK_URI = "http://localhost:8080/candidates/oauth";

	Resource gdSecretKeys = new ClassPathResource("key/client_secret.json");

	private static GoogleAuthorizationCodeFlow flow;

	@PostConstruct
	public void init() throws Exception {

		GoogleClientSecrets secrets = GoogleClientSecrets.load(JSON_FACTORY,
				new InputStreamReader(gdSecretKeys.getInputStream()));

		flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY, secrets, SCOPES)
				.setDataStoreFactory(new FileDataStoreFactory(new java.io.File("c:\\db\\cred"))).build();

	}

	@GetMapping("/googlesignout")
	public ResponseEntity<String> deleteCred() {
		java.io.File cred = new java.io.File("c:\\db\\cred\\StoredCredential");
		String result = "";
		if (cred.delete()) {
			result = "signed out";
		} else {
			result = "already signed out";
		}

		return new ResponseEntity<String>(result, HttpStatus.ACCEPTED);
	}

	@GetMapping(value = { "/" })
	public ResponseEntity<String> test(HttpServletRequest request) throws Exception {
		boolean isUserAuthenticated = false;
		String result = "token not valid";
		List<String> list = (List<String>) request.getSession().getAttribute("MY_SESSION_MESSAGES");
		String USER_IDENTIFIER_KEY = list.get(list.size() - 1);
		Credential credential = flow.loadCredential(USER_IDENTIFIER_KEY);

		if (credential != null) {
			boolean tokenValid = credential.refreshToken();
			if (tokenValid) {
				isUserAuthenticated = true;
				result = "token valid";
			}
		}

		return new ResponseEntity<String>(result, HttpStatus.ACCEPTED);
	}

	@GetMapping(value = { "/googlesignin" })
	public void doGoogleSignIn(HttpServletResponse response) throws Exception {
		GoogleAuthorizationCodeRequestUrl url = flow.newAuthorizationUrl();
		String redirectURL = url.setRedirectUri(CALLBACK_URI).setAccessType("offline").build();
		response.sendRedirect(redirectURL);

	}

	@GetMapping(value = { "/oauth" })
	public ResponseEntity<String> saveAuthorizationCode(HttpServletRequest request) throws Exception {
		String result = "token not save";

		String USER_IDENTIFIER_KEY = request.getSession().getAttribute("email").toString();
		String code = request.getParameter("code");
		if (code != null) {
			saveToken(code, USER_IDENTIFIER_KEY);
			result = "token saved";
		}
		return new ResponseEntity<String>(result, HttpStatus.ACCEPTED);
	}

	private void saveToken(String code, String id) throws Exception {
		GoogleTokenResponse response = flow.newTokenRequest(code).setRedirectUri(CALLBACK_URI).execute();

		flow.createAndStoreCredential(response, id);
	}

	@GetMapping("/load")
	public ResponseEntity<Object> loadOptions() {
		LoadOptionsDto result = new LoadOptionsDto();
		try {
			 result=candidateService.loadOptions();
			 return ResponseHandler.generateResponseWithEntity("Data retrieved Successfully!", HttpStatus.OK, result);

		} catch (Exception e) {
			return ResponseHandler.generateResponse("Bad Request!", HttpStatus.BAD_REQUEST);

		}
		
	}
	@GetMapping("/load/source")
	public ResponseEntity<Object> loadSourceOptions() {
		List<CandidateSource> source = new ArrayList<>();
		try {
			source=sourceRepo.findAll();
			if(!source.isEmpty()) {
				return ResponseHandler.generateResponseWithEntity("Data retrieved Successfully!", HttpStatus.OK, source);
			}else {
				return ResponseHandler.generateResponse("No record found", HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return ResponseHandler.generateResponse("Bad Request!", HttpStatus.BAD_REQUEST);

		}
	}
	@GetMapping("/load/technology")
	public ResponseEntity<Object> loadTechnologyOptions() {
		List<Technology> technology= new ArrayList<>();
		try {
			technology=technologyRepo.findAll();
			if(!technology.isEmpty()) {
				return ResponseHandler.generateResponseWithEntity("Data retrieved Successfully!", HttpStatus.OK, technology);
			}else {
				return ResponseHandler.generateResponse("No record found", HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return ResponseHandler.generateResponse("Bad Request!", HttpStatus.BAD_REQUEST);

		}
	}
	
	@GetMapping("/load/status")
	public ResponseEntity<Object> loadStatusOptions() {
		List<CandidateStatus> status= new ArrayList<>();
		try {
			status=statusRepo.findAll();
			if(!status.isEmpty()) {
				return ResponseHandler.generateResponseWithEntity("Data retrieved Successfully!", HttpStatus.OK, status);
			}else {
				return ResponseHandler.generateResponse("No record found", HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return ResponseHandler.generateResponse("Bad Request!", HttpStatus.BAD_REQUEST);

		}
	}
	@GetMapping("/load/jobtitle")
	public ResponseEntity<Object> loadJobTitleOptions() {
		List<String> jobTitle= new ArrayList<>();
		try {
			jobTitle=candidateRepo.getJobTitles();
			if(!jobTitle.isEmpty()) {
				return ResponseHandler.generateResponseWithEntity("Data retrieved Successfully!", HttpStatus.OK, jobTitle);
			}else {
				return ResponseHandler.generateResponse("No record found", HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return ResponseHandler.generateResponse("Bad Request!", HttpStatus.BAD_REQUEST);

		}
	}

	@PostMapping({ "/add" })
	public ResponseEntity<Object> add(@RequestBody AddCandidateDto candidate, HttpServletRequest request)
			throws Exception {
		try {

			String USER_IDENTIFIER_KEY;
			try {
				USER_IDENTIFIER_KEY = request.getSession().getAttribute("email").toString();
			} catch (Exception e) {
				return ResponseHandler.generateResponse("no current session, please login ", HttpStatus.UNAUTHORIZED);
			}

			if (candidate.getFirstName().equals("") || candidate.getLastName().equals("")
					|| candidate.getMiddleName().equals("") || candidate.getMobileNo().equals("")
					|| candidate.getEmail().equals("") || candidate.getJobTitle().equals("")
					|| candidate.getTotalYearsExp() == null) {
				return ResponseHandler.generateResponse("Please fill all possible values", HttpStatus.BAD_REQUEST);
			} else if (candidateRepo.existsByEmail(candidate.getEmail())) {
				return ResponseHandler.generateResponse("Email already exists", HttpStatus.FOUND);
			} else if (candidateRepo.existsByMobileNo(candidate.getMobileNo())) {
				return ResponseHandler.generateResponse("Mobile number already exists", HttpStatus.FOUND);
			} else {

				if (candidate.getTechnologies() != null) {
					for (TechnologiesDto t : candidate.getTechnologies()) {
						if (t.getTechnologyId() != null) {
							if (!technologyRepo.existsById(t.getTechnologyId())) {
								return ResponseHandler.generateResponse(
										"Technology " + t.getTechnologyId() + " does not exist", HttpStatus.NOT_FOUND);
							}
						}
					}
				}
				if (candidate.getSourceId() != null) {
					if (!sourceRepo.existsById(candidate.getSourceId()))
						return ResponseHandler.generateResponse(
								"Source " + candidate.getSourceId() + " does not exists", HttpStatus.NOT_FOUND);
				}
				if (candidate.getNotes().getStatusId() != null) {
					if (!statusRepo.existsById(candidate.getNotes().getStatusId()))
						return ResponseHandler.generateResponse(
								"Status " + candidate.getNotes().getStatusId() + " does not exist",
								HttpStatus.NOT_FOUND);
				}

				// from input
				List<CandidateFile> cvs = new ArrayList<>();
				if (candidate.getFiles() != null) {
					for (FilesDto f : candidate.getFiles()) {
						CandidateFile addFile = new CandidateFile();
						addFile.setPath(f.getPath());
						FileType fType = new FileType();
						fType.setId(f.getFileTypeId());
						addFile.setFileType(fType);
						cvs.add(addFile);
					}
				}

				// list of cvs
				List<CandidateFile> newCv = new ArrayList<>();
				if (!cvs.isEmpty()) {
					// get email from session
					Credential cred = flow.loadCredential(USER_IDENTIFIER_KEY);

					Drive drive = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, cred)
							.setApplicationName("GoogleDriveSpringboot").build();

					// create folder
					File parentFolder = new File();
					parentFolder.setName(candidate.getFirstName() + " " + candidate.getLastName());
					parentFolder.setMimeType("application/vnd.google-apps.folder");

					// shared folder of hr
					parentFolder.setParents(Arrays.asList("17lsj-syjE8S22eHd6MeWTH3c49ucnzdj"));
					File folder = drive.files().create(parentFolder).setFields("id").execute();

					// get folder id
					String folderId = folder.getId();

					/*
					 * Permission permission = new Permission(); permission.setType("anyone");
					 * permission.setRole("organizer");
					 */

					for (CandidateFile cvFile : cvs) {

						// get file type name
						FileType fileType = new FileType();
						fileType = fileTypeRepo.findById(cvFile.getFileType().getId());
						String typeName = fileType.getType();

						CandidateFile doc = new CandidateFile();

						// create cv
						File cv = new File();
						cv.setName(candidate.getFirstName() + " " + candidate.getLastName() + " " + typeName);
						cv.setParents(Arrays.asList(folderId));

						FileContent content = new FileContent("application/msword", new java.io.File(cvFile.getPath()));
						// upload cv
						File uploadedFile = drive.files().create(cv, content).setFields("id,webViewLink").execute();

						// get cv id
						String fileId = uploadedFile.getId();
						String link = uploadedFile.getWebViewLink();
						doc.setLink(link);
						doc.setFolderId(folderId);
						doc.setFileId(fileId);
						doc.setFileType(cvFile.getFileType());
						newCv.add(doc);
					}
				}

				candidateService.addCandidate(candidate, USER_IDENTIFIER_KEY, newCv);
				return ResponseHandler.generateResponse("Candidate has been addedd successfully", HttpStatus.OK);
			}

		} catch (NullPointerException e) {
			return ResponseHandler.generateResponse("There is a missing field for request!", HttpStatus.BAD_REQUEST);

		}
	}

	@PutMapping("/update/{id}")
	public ResponseEntity<Object> updateCandidate(@PathVariable("id") Long id,
			@RequestBody UpdateCandidateDto candidate, HttpServletRequest request) throws IOException {

		try {

			String USER_IDENTIFIER_KEY;
			try {
				USER_IDENTIFIER_KEY = request.getSession().getAttribute("email").toString();
			} catch (Exception e) {
				return ResponseHandler.generateResponse("no current session, please login ", HttpStatus.UNAUTHORIZED);
			}

			if (!candidateRepo.existsById(id)) {
				return ResponseHandler.generateResponse("Candidate does not exists ", HttpStatus.NOT_FOUND);

			} else {
				if (candidate.getEmail() != null) {
					if (candidateRepo.existsByEmail(candidate.getEmail())) {
						return ResponseHandler.generateResponse("Email already exists", HttpStatus.FOUND);
					}
				}
				if (candidate.getMobileNo() != null) {
					if (candidateRepo.existsByMobileNo(candidate.getMobileNo())) {
						return ResponseHandler.generateResponse("Mobile number already exists", HttpStatus.FOUND);
					}
				}
				if (candidate.getSourceId() != null) {
					if (!sourceRepo.existsById(candidate.getSourceId())) {
						return ResponseHandler.generateResponse(
								"Source " + candidate.getSourceId() + " does not exists", HttpStatus.NOT_FOUND);
					}
				}
				if (candidate.getNotes() != null) {
					if (candidate.getNotes().getStatusId() != null) {
						if (!statusRepo.existsById(candidate.getNotes().getStatusId())) {
							return ResponseHandler.generateResponse(
									"Status " + candidate.getNotes().getStatusId() + " does not exist",
									HttpStatus.NOT_FOUND);
						}
					}
				}
				if (candidate.getTechnologies() != null) {
					for (com.xurpas.thirdparty.talent.solutions.payload.UpdateCandidateDto.TechnologiesDto t : candidate
							.getTechnologies()) {
						if (t.getTechnologyId() != null) {
							if (!technologyRepo.existsById(t.getTechnologyId())) {
								return ResponseHandler.generateResponse(
										"Technology " + t.getTechnologyId() + " does not exist", HttpStatus.NOT_FOUND);
							}
						}

					}
				}

				// from input
				List<CandidateFile> updatedCvs = new ArrayList<>();
				if (candidate.getFiles() != null) {
					for (com.xurpas.thirdparty.talent.solutions.payload.UpdateCandidateDto.FilesDto f : candidate
							.getFiles()) {
						CandidateFile addFile = new CandidateFile();
						addFile.setPath(f.getPath());
						FileType fType = new FileType();
						fType.setId(f.getFileTypeId());
						addFile.setFileType(fType);
						updatedCvs.add(addFile);
					}
				}
				List<CandidateFile> existingCvs = candidateRepo.getById(id).getCandidateFile();

				List<CandidateFile> newCv = new ArrayList<>();
				if (!updatedCvs.isEmpty()) {

					// get email from session

					Credential cred = flow.loadCredential(USER_IDENTIFIER_KEY);

					Drive drive = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, cred)
							.setApplicationName("GoogleDriveSpringboot").build();

					for (CandidateFile cvFile : updatedCvs) {
						// get existing file with type
						CandidateFile existingFileType = checkExistingType(existingCvs, cvFile.getFileType().getId());

						String folderId = fileRepo.getFileByCandidateId(id).getFolderId();
						// get file type name
						String typeName = fileTypeRepo.findById(cvFile.getFileType().getId()).getType();

						CandidateFile doc = new CandidateFile();

						// create cv
						File cv = new File();
						cv.setName(candidateRepo.getById(id).getFirstName() + " "
								+ candidateRepo.getById(id).getLastName() + " " + typeName);
						cv.setParents(Arrays.asList(folderId));

						FileContent content = new FileContent("application/msword", new java.io.File(cvFile.getPath()));

						if (existingFileType.getFileId() != null) {

							// check if the user is the owner of the file
							PermissionList permissionList = drive.permissions().list(existingFileType.getFileId())
									.execute();
							String fileOwnerId = "";

							for (Permission perm : permissionList.getPermissions()) {
								if (perm.getRole().equals("owner"))
									fileOwnerId = perm.getId();
							}

							About about = drive.about().get().setFields("*").execute();
							if (fileOwnerId.equals(about.getUser().getPermissionId())) {
								// delete old file
								drive.files().delete(existingFileType.getFileId()).execute();
								// upload cv
								File uploadedFile = drive.files().create(cv, content).setFields("id,webViewLink")
										.execute();

								// get cv id
								String fileId = uploadedFile.getId();
								String link = uploadedFile.getWebViewLink();
								doc.setLink(link);
								doc.setFileId(fileId);
								doc.setId(existingFileType.getId());
								newCv.add(doc);
							} else {
								return ResponseHandler.generateResponse(
										"you can only update a file if you are the owner. google drive api error",
										HttpStatus.CONFLICT);
							}

						} else {
							// upload cv
							File uploadedFile = drive.files().create(cv, content).setFields("id,webViewLink").execute();

							// get cv id
							String fileId = uploadedFile.getId();
							String link = uploadedFile.getWebViewLink();
							doc.setLink(link);
							doc.setFileId(fileId);
							doc.setFolderId(folderId);
							doc.setFileType(cvFile.getFileType());
							newCv.add(doc);
						}
					}
				}
				String note = candidateService.updateCandidate(id, candidate, newCv, USER_IDENTIFIER_KEY);
				return ResponseHandler.generateResponse("Candidate updated successfully!", HttpStatus.OK);
			}
		} catch (Exception e) {
			return ResponseHandler.generateResponse("Bad request", HttpStatus.BAD_REQUEST);
		}

	}

	private CandidateFile checkExistingType(List<CandidateFile> file, int typeId) {
		CandidateFile result = new CandidateFile();
		for (CandidateFile cvFile : file) {
			if (cvFile.getFileType().getId() == typeId) {
				result.setFileId(cvFile.getFileId());
				result.setId(cvFile.getId());
				return result;
			}
		}
		return result;
	}

	@GetMapping(value = { "/listfiles" }, produces = { "application/json" })
	public @ResponseBody List<FileItem> listFiles(HttpServletRequest request) throws Exception {
		String USER_IDENTIFIER_KEY = request.getSession().getAttribute("email").toString();
		Credential cred = flow.loadCredential(USER_IDENTIFIER_KEY);
		Drive drive = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, cred).setApplicationName("GoogleDriveSpringboot")
				.build();
		List<FileItem> responseList = new ArrayList<>();

		FileList fileList = drive.files().list().setFields("files(id,name,mimeType,driveId)").execute();
		for (File file : fileList.getFiles()) {
			FileItem item = new FileItem();
			item.setId(file.getId());
			item.setName(file.getName());
			item.setMime(file.getMimeType());
			item.setDriveId(file.getDriveId());
			responseList.add(item);
		}

		return responseList;
	}

	@GetMapping("/test/{id}")
	public ResponseEntity<GetCandidateProjection> getCandidate(@PathVariable("id") Long id) {
		return new ResponseEntity<GetCandidateProjection>(candidateService.getCandidateById(id), HttpStatus.OK);

	}

	@GetMapping("/{id}")
	public ResponseEntity<Object> getCandidateById(@PathVariable("id") Long id) {

		if (candidateRepo.existsById(id)) {
			GetCandidateDto result = candidateService.getCandidate(id);
			return ResponseHandler.generateResponseWithEntity("Data retrieved Successfully!", HttpStatus.OK, result);

		} else {
			return ResponseHandler.generateResponse("Candidate not exists", HttpStatus.NOT_FOUND);

		}

	}

	@GetMapping("/file/{id}")
	public void downloadFile(HttpServletRequest request, @PathVariable("id") String id, HttpServletResponse response)
			throws IOException {

		Drive drive = getDriveService(request);
		String name = drive.files().get(id).execute().getName();
		System.out.println(name);
		String mimeType = drive.files().get(id).execute().setName(name).getMimeType();

		response.setContentType(mimeType);
		drive.files().get(id).executeMediaAndDownloadTo(response.getOutputStream());

	}

	public static Drive getDriveService(HttpServletRequest request) throws IOException {
		String USER_IDENTIFIER_KEY = request.getSession().getAttribute("email").toString();
		Credential cred = flow.loadCredential(USER_IDENTIFIER_KEY);
		return new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, cred).setApplicationName("GoogleDriveSpringboot")
				.build();
	}

}
