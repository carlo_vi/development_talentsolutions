package com.xurpas.thirdparty.talent.solutions.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xurpas.thirdparty.talent.solutions.model.Role;

import java.util.List;
import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByroleName(String name);
    //Role findByroleName(String name);
	//<T> void saveAll(List<? extends T> hr, List<? extends T> tr);
}