package com.xurpas.thirdparty.talent.solutions.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.MutableSortDefinition;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.stereotype.Service;

import com.xurpas.thirdparty.talent.solutions.payload.RecruiterActivitySummaryDto;
import com.xurpas.thirdparty.talent.solutions.payload.RecruiterActivitySummaryPaginationDto;
import com.xurpas.thirdparty.talent.solutions.payload.RecruiterCandidatesTotalDto;
import com.xurpas.thirdparty.talent.solutions.payload.RecruiterTestProjection;
import com.xurpas.thirdparty.talent.solutions.payload.SearchDto;
import com.xurpas.thirdparty.talent.solutions.payload.SearchPaginationDto;
import com.xurpas.thirdparty.talent.solutions.payload.SearchResultProjection;
import com.xurpas.thirdparty.talent.solutions.repository.RecruiterRepository;

@Service
public class RecruiterService {
	@Autowired
	private RecruiterRepository recruiterRepo;

	public List<RecruiterActivitySummaryDto> getSummary() {

		List<RecruiterActivitySummaryDto> summary = new ArrayList<>();

		for (Object[] obj : recruiterRepo.getSummary()) {
			RecruiterActivitySummaryDto test = new RecruiterActivitySummaryDto();
			test.setId((BigInteger) (obj[0]));
			test.setName(String.valueOf(obj[1]));
			test.setTotal_entries_historical((BigInteger) (obj[2]));
			test.setLast_activity((Date) (obj[3]));
			test.setTotal_entries_this_week((BigInteger) (obj[4]));
			summary.add(test);
		}
		return summary;
	}

//	public RecruiterActivitySummaryPaginationDto getPage(Integer page, Integer size, String sortField,
//			String sortType) {
//
//		List<RecruiterActivitySummaryDto> summary = new ArrayList<>();
//
//		for (Object[] obj : recruiterRepo.getSummary()) {
//			RecruiterActivitySummaryDto test = new RecruiterActivitySummaryDto();
//			test.setId((BigInteger) (obj[0]));
//			test.setName(String.valueOf(obj[1]));
//			test.setTotal_entries_historical((BigInteger) (obj[2]));
//			test.setLast_activity((Date) (obj[3]));
//			test.setTotal_entries_this_week((BigInteger) (obj[4]));
//			summary.add(test);
//		}
//		RecruiterCandidatesTotalDto total = new RecruiterCandidatesTotalDto();
//		for (Object[] obj : recruiterRepo.getTotal()) {
//			total.setAllTotal((BigInteger) (obj[0]));
//			total.setAllTotalWeek((BigInteger) (obj[1]));
//
//		}
//		if (size > summary.size())
//			size = summary.size();
//		int totalpages = summary.size() / size;
//
//		int max = (page - 1) >= totalpages ? summary.size() : size * (page);
//		int min = (page - 1) > totalpages ? max : size * (page - 1);
//
//		PagedListHolder<RecruiterActivitySummaryDto> pageResponse = new PagedListHolder<RecruiterActivitySummaryDto>(
//				summary.subList(min, max));
//
//		boolean sort = true;
//		if (sortType.equalsIgnoreCase("ASC")) {
//			sort = true;
//		} else {
//			sort = false;
//		}
//		MutableSortDefinition x = new MutableSortDefinition(sortField, true, sort);
//		// pageResponse.setPage(page-1);
//		pageResponse.setPageSize(size);
//		pageResponse.setSort(x);
//		pageResponse.resort();
//
//		int totalElements = summary.size();
//		long totalPages = totalElements / size;
//
//		if (size > totalElements) {
//			totalPages = 1;
//		}
//		if (totalPages == 1) {
//			if (size < totalElements) {
//				totalPages++;
//			}
//		}
//
//		return new RecruiterActivitySummaryPaginationDto(pageResponse.getPageList(), total, totalElements, totalPages,
//				page);
//	}
	public RecruiterActivitySummaryPaginationDto getRecruiter(String searchUser, Integer page, Integer size,
			String sortField, String sortType) {
		Sort.Direction sort = sortType.equalsIgnoreCase("desc") ? Sort.Direction.DESC
				: Sort.Direction.ASC;
		RecruiterCandidatesTotalDto total = new RecruiterCandidatesTotalDto();
		for (Object[] obj : recruiterRepo.getTotal()) {
			total.setAllTotal((BigInteger) (obj[0]));
			total.setAllTotalWeek((BigInteger) (obj[1]));

		}
		
		Pageable pageable = PageRequest.of(page - 1, size, JpaSort.unsafe(
				sort
				, "("+sortField+")"));
		Page<RecruiterTestProjection> toPage=null;
		
		if (!searchUser.isEmpty()) {
			toPage= recruiterRepo.getRecruiterByName(searchUser,pageable);
		}else {
			toPage= recruiterRepo.getRecruiters(pageable);
		}
		
		List<RecruiterTestProjection> content = toPage.getContent();
		long totalElements = toPage.getTotalElements();
		long totalPages = toPage.getTotalPages();
		int currentPage = toPage.getNumber() + 1;
		return new RecruiterActivitySummaryPaginationDto(content, total, totalElements,
				totalPages, currentPage);
		
	}
//	public RecruiterActivitySummaryPaginationDto getPageSearch(String searchUser, Integer page, Integer size,
//			String sortField, String sortType) {
//
//		List<RecruiterActivitySummaryDto> summary = new ArrayList<>();
//		List<Object[]> recruiters = new ArrayList<>();
//		RecruiterCandidatesTotalDto total = new RecruiterCandidatesTotalDto();
//		if (!searchUser.isEmpty()) {
//			recruiters = recruiterRepo.searchRecruiter(searchUser);
//		} else {
//			recruiters = recruiterRepo.getSummary();
//		}
//		if (!recruiters.isEmpty()) {
//			for (Object[] obj : recruiters) {
//				RecruiterActivitySummaryDto test = new RecruiterActivitySummaryDto();
//				test.setId((BigInteger) (obj[0]));
//				test.setName(String.valueOf(obj[1]));
//				test.setTotal_entries_historical((BigInteger) (obj[2]));
//				test.setLast_activity((Date) (obj[3]));
//				test.setTotal_entries_this_week((BigInteger) (obj[4]));
//				summary.add(test);
//			}
//
//			for (Object[] obj : recruiterRepo.getTotal()) {
//				total.setAllTotal((BigInteger) (obj[0]));
//				total.setAllTotalWeek((BigInteger) (obj[1]));
//
//			}
//			if (size > summary.size())
//				size = summary.size();
//			int totalpages = summary.size() / size;
//
//			int max = (page - 1) >= totalpages ? summary.size() : size * (page);
//			int min = (page - 1) > totalpages ? max : size * (page - 1);
//
//			PagedListHolder<RecruiterActivitySummaryDto> pageResponse = new PagedListHolder<RecruiterActivitySummaryDto>(
//					summary.subList(min, max));
//
//			boolean sort = true;
//			if (sortType.equalsIgnoreCase("ASC")) {
//				sort = true;
//			} else {
//				sort = false;
//			}
//			MutableSortDefinition x = new MutableSortDefinition(sortField, true, sort);
//			// pageResponse.setPage(page-1);
//			pageResponse.setPageSize(size);
//			pageResponse.setSort(x);
//			pageResponse.resort();
//
//			int totalElements = summary.size();
//			long totalPages = totalElements / size;
//
//			if (size > totalElements) {
//				totalPages = 1;
//			}
//			if (totalPages == 1) {
//				if (size < totalElements) {
//					totalPages++;
//				}
//			}
//
//			return new RecruiterActivitySummaryPaginationDto(pageResponse.getPageList(), total, totalElements,
//					totalPages, page);
//		} else {
//			return new RecruiterActivitySummaryPaginationDto(summary, total, 0, 0, page);
//		}
//	}

}
