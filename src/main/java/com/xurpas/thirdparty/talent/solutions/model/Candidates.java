package com.xurpas.thirdparty.talent.solutions.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.xurpas.thirdparty.talent.solutions.payload.CVDto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "candidates")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Candidates {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long candidate_id;

	@Column(name = "first_name", nullable = false, length = 50)
	@NotEmpty
	private String firstName;

	@Column(name = "last_name", nullable = false, length = 50)
	@NotEmpty
	private String lastName;

	@Column(name = "middle_name", nullable = false, length = 50)
	@NotEmpty
	private String middleName;

	@Column(name = "mobile_no", nullable = false, length = 20, unique = true)
	@NotEmpty
	private String mobileNo;

	@Column(name = "email", nullable = false, length = 50, unique = true)
	@Email
	private String email;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "source_id", columnDefinition = "integer")
	private CandidateSource candidateSource;

	@Column(name = "job_title", length = 50)
	private String jobTitle;

	@Column(name = "job_type", length = 10)
	private String jobType;

	@Column(name = "total_years_exp")
	private Integer totalYearsExp;

	@Column(name = "current_salary")
	private Double currentSalary;

	@Column(name = "asking_salary")
	private Double askingSalary;

	@Column(name = "course", length = 100)
	private String course;

	@Column(name = "address", length = 200)
	private String address;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date_created")
	private Date dateCreated;

	@PrePersist
	private void onCreate() {
		dateCreated = new Date();
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id")
	private User userAccount;

	@Column(name = "school", length = 200)
	private String school;

	@Column(name = "year_graduated")
	private Integer yearGraduated;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "candidate_id")
	private List<CandidateTechnology> technology= new ArrayList<>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "candidate_id")
	private List<CandidateNotes> candidateNotes = new ArrayList<>();

	
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "candidate_id")
	private List<CandidateFile> candidateFile= new ArrayList<>();
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="current_status")
	private CandidateStatus currentStatus;
}
