package com.xurpas.thirdparty.talent.solutions.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xurpas.thirdparty.talent.solutions.model.CandidateSource;

@Repository
public interface CandidateSourceRepository extends JpaRepository<CandidateSource, Integer>{

	boolean existsById(Integer id);
}
