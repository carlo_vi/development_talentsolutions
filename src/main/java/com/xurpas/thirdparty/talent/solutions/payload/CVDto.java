package com.xurpas.thirdparty.talent.solutions.payload;

import com.xurpas.thirdparty.talent.solutions.model.FileType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CVDto {
	private String path;
	private FileType type;
}
