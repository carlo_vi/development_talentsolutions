package com.xurpas.thirdparty.talent.solutions.payload;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;

import com.xurpas.thirdparty.talent.solutions.model.User;

public interface SearchResultProjection {
	Long getCandidate_id();

	//@Value("#{target.firstName+' '+target.lastName}")
	String getFullName();

	String getJobTitle();

	Integer getTotalYearsExp();

	String getOwner();
//	owner getUserAccount();

//	List<notes> getCandidateNotes();
//
//	source getCandidateSource();
//
//	List<techs> getTechnology();

//	interface owner {
//		String getFirstName();
//	}

//	interface source {
//		String getSourceName();
//	}
//
//	interface notes {
//		status getCandidateStatus();
//
//		interface status {
//			String getStatus();
//		}
//	}
//
//	interface techs {
//		tech getTechnology();
//
//		interface tech {
//			String getTechnologyName();
//		}
//
//	}
}
