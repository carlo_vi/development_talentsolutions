package com.xurpas.thirdparty.talent.solutions;

import javax.sql.DataSource;

import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.integration.IntegrationDataSourceScriptDatabaseInitializer;
import org.springframework.boot.sql.init.DatabaseInitializationSettings;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class TalentSolutionsApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(TalentSolutionsApplication.class, args);
	}
	
	
}
