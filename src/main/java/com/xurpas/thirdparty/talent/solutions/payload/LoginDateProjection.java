package com.xurpas.thirdparty.talent.solutions.payload;

import java.util.Date;

public interface LoginDateProjection {

	void save(AskingSalaryProjection last_login_date);

	Date setLast_login_date();
}
