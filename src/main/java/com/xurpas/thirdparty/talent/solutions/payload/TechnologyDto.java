package com.xurpas.thirdparty.talent.solutions.payload;

import java.math.BigInteger;

import lombok.Data;

@Data
public class TechnologyDto {
	String technologyName;
	BigInteger count;
}
