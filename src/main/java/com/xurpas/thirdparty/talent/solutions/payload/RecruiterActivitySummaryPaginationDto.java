package com.xurpas.thirdparty.talent.solutions.payload;


import java.util.List;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RecruiterActivitySummaryPaginationDto {
	private List<RecruiterTestProjection> recruiters;
	private RecruiterCandidatesTotalDto total;
	private long totalElements;
	private long totalPages;
	private int currentPage;

	

}
