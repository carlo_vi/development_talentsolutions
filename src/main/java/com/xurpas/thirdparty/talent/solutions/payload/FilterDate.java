package com.xurpas.thirdparty.talent.solutions.payload;

public enum FilterDate {
	day, month, year;
}
