package com.xurpas.thirdparty.talent.solutions.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xurpas.thirdparty.talent.solutions.model.FileType;

@Repository
public interface FileTypeRepository extends JpaRepository<FileType, Integer>{

	FileType findById(int param);
}
