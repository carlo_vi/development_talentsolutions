package com.xurpas.thirdparty.talent.solutions.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.xurpas.thirdparty.talent.solutions.exception.ResponseHandler;
import com.xurpas.thirdparty.talent.solutions.model.CandidateFile;
import com.xurpas.thirdparty.talent.solutions.model.CandidateNotes;
import com.xurpas.thirdparty.talent.solutions.model.CandidateSource;
import com.xurpas.thirdparty.talent.solutions.model.CandidateStatus;
import com.xurpas.thirdparty.talent.solutions.model.CandidateTechnology;
import com.xurpas.thirdparty.talent.solutions.model.Candidates;
import com.xurpas.thirdparty.talent.solutions.model.FileType;
import com.xurpas.thirdparty.talent.solutions.model.Technology;
import com.xurpas.thirdparty.talent.solutions.model.User;
import com.xurpas.thirdparty.talent.solutions.payload.AddCandidateDto;
import com.xurpas.thirdparty.talent.solutions.payload.AddCandidateDto.NotesDto;
import com.xurpas.thirdparty.talent.solutions.payload.AddCandidateDto.TechnologiesDto;
import com.xurpas.thirdparty.talent.solutions.payload.GetCandidateDto;
import com.xurpas.thirdparty.talent.solutions.payload.GetCandidateDto.FilesDto;
import com.xurpas.thirdparty.talent.solutions.payload.GetCandidateDto.GetNotesDto;
import com.xurpas.thirdparty.talent.solutions.payload.GetCandidateDto.GetTechnologyDto;
import com.xurpas.thirdparty.talent.solutions.payload.GetCandidateProjection;
import com.xurpas.thirdparty.talent.solutions.payload.GetCandidateProjection.files;
import com.xurpas.thirdparty.talent.solutions.payload.GetCandidateProjection.notes;
import com.xurpas.thirdparty.talent.solutions.payload.GetCandidateProjection.techs;
import com.xurpas.thirdparty.talent.solutions.payload.LoadOptionsDto;
import com.xurpas.thirdparty.talent.solutions.payload.UpdateCandidateDto;
import com.xurpas.thirdparty.talent.solutions.repository.CandidateFileRepository;
import com.xurpas.thirdparty.talent.solutions.repository.CandidateRepository;
import com.xurpas.thirdparty.talent.solutions.repository.CandidateSourceRepository;
import com.xurpas.thirdparty.talent.solutions.repository.CandidateStatusRepository;
import com.xurpas.thirdparty.talent.solutions.repository.CandidateTechnologyRepository;
import com.xurpas.thirdparty.talent.solutions.repository.FileTypeRepository;
import com.xurpas.thirdparty.talent.solutions.repository.TechnologyRepository;
import com.xurpas.thirdparty.talent.solutions.repository.UserRepository;

@Service

public class CandidateService {

	@Autowired
	CandidateRepository candidateRepo;
	@Autowired
	CandidateStatusRepository statusRepo;
	@Autowired
	TechnologyRepository technologyRepo;
	@Autowired
	CandidateSourceRepository sourceRepo;
	@Autowired
	CandidateFileRepository fileRepo;
	@Autowired
	UserRepository userAccountRepo;
	@Autowired
	CandidateTechnologyRepository candidateTechnologyRepo;
	@Autowired
	FileTypeRepository fileTypeRepo;

	public void addCandidate(AddCandidateDto newCandidate, String email, List<CandidateFile> cvs) {
		Candidates candidate = new Candidates();
		CandidateSource source = new CandidateSource();
		User userId = new User();

		// id from session

		long emailToId = userAccountRepo.findByEmail(email).get().getUser_id();
		userId.setUser_id(emailToId);

		candidate.setFirstName(newCandidate.getFirstName());
		candidate.setLastName(newCandidate.getLastName());
		candidate.setMiddleName(newCandidate.getMiddleName());
		candidate.setMobileNo(newCandidate.getMobileNo());
		candidate.setEmail(newCandidate.getEmail());

		candidate.setJobTitle(newCandidate.getJobTitle());
		candidate.setJobType(newCandidate.getJobType());
		candidate.setTotalYearsExp(newCandidate.getTotalYearsExp());
		candidate.setCurrentSalary(newCandidate.getCurrentSalary());
		candidate.setAskingSalary(newCandidate.getAskingSalary());
		candidate.setCourse(newCandidate.getCourse());
		candidate.setAddress(newCandidate.getAddress());
		candidate.setSchool(newCandidate.getSchool());
		candidate.setYearGraduated(newCandidate.getYearGraduated());
		candidate.setUserAccount(userId);

		if (newCandidate.getSourceId() != null) {
			source.setSourceId(newCandidate.getSourceId());
			candidate.setCandidateSource(source);
		}
		if (newCandidate.getTechnologies() != null) {
			for (TechnologiesDto t : newCandidate.getTechnologies()) {
				if (t.getTechnologyId() != null) {
					CandidateTechnology technology = new CandidateTechnology();
					Technology tech = new Technology();
					tech.setTechnology_id(t.getTechnologyId());
					technology.setTechnology(tech);
					technology.setYearExp(t.getYearExperience());
					candidate.getTechnology().add(technology);
				}

			}
		}

		if (newCandidate.getNotes() != null) {

			if (newCandidate.getNotes().getStatusId() != null) {
				CandidateNotes notes = new CandidateNotes();
				CandidateStatus status = new CandidateStatus();

				status.setStatus_id(newCandidate.getNotes().getStatusId());
				notes.setCandidateStatus(status);
				notes.setNote(newCandidate.getNotes().getNote());
				notes.setUserAccount(userId);

				candidate.getCandidateNotes().add(notes);
				candidate.setCurrentStatus(status);
			}

		}

		candidate.setCandidateFile(cvs);

		candidateRepo.save(candidate);
	}

	public LoadOptionsDto loadOptions() {
		LoadOptionsDto load = new LoadOptionsDto();
		load.setStatus(statusRepo.findAll());
		load.setSource(sourceRepo.findAll());
		load.setTechnology(technologyRepo.findAll());
		
		load.setJobTitle(candidateRepo.getJobTitles());
		return load;
	}

	public void update(Integer id, Candidates updatedCandidate) {

		updatedCandidate.setCandidate_id(id);
		candidateRepo.save(updatedCandidate);
	}

	@Transactional
	public String updateCandidate(Long id, UpdateCandidateDto updatedCandidate, List<CandidateFile> newCv,
			String email) {

		String note = "";
		Candidates existingCandidate = candidateRepo.getById(id);

		if (existingCandidate != null) {
			if (updatedCandidate.getFirstName() != null) {
				note += "First name: from " + existingCandidate.getFirstName() + " to "
						+ updatedCandidate.getFirstName() + " .\n";
				existingCandidate.setFirstName(updatedCandidate.getFirstName());
			}
			if (updatedCandidate.getLastName() != null) {
				note += "Last name: from " + existingCandidate.getLastName() + " to " + updatedCandidate.getLastName()
						+ " .\n";
				existingCandidate.setLastName(updatedCandidate.getLastName());
			}
			if (updatedCandidate.getMiddleName() != null) {
				note += "Midle name: from " + existingCandidate.getMiddleName() + " to "
						+ updatedCandidate.getMiddleName() + " .\n";
				existingCandidate.setMiddleName(updatedCandidate.getMiddleName());
			}
			if (updatedCandidate.getMobileNo() != null) {
				note += "Mobile no.: from " + existingCandidate.getMobileNo() + " to " + updatedCandidate.getMobileNo()
						+ " .\n";
				existingCandidate.setMobileNo(updatedCandidate.getMobileNo());
			}
			if (updatedCandidate.getEmail() != null) {
				note += "Email: from " + existingCandidate.getEmail() + " to " + updatedCandidate.getEmail() + " .\n";
				existingCandidate.setEmail(updatedCandidate.getEmail());
			}
			if (updatedCandidate.getAddress() != null) {
				note += "Address: from " + existingCandidate.getAddress() + " to " + updatedCandidate.getAddress()
						+ " .\n";
				existingCandidate.setAddress(updatedCandidate.getAddress());
			}
			if (updatedCandidate.getCurrentSalary() != null) {
				note += "Current salary: from " + existingCandidate.getCurrentSalary() + " to "
						+ updatedCandidate.getCurrentSalary() + " .\n";
				existingCandidate.setCurrentSalary(updatedCandidate.getCurrentSalary());
			}
			if (updatedCandidate.getAskingSalary() != null) {
				note += "Asking salary: from " + existingCandidate.getAskingSalary() + " to "
						+ updatedCandidate.getAskingSalary() + " .\n";
				existingCandidate.setAskingSalary(updatedCandidate.getAskingSalary());
			}
			if (updatedCandidate.getCourse() != null) {
				note += "Course: from " + existingCandidate.getCourse() + " to " + updatedCandidate.getCourse()
						+ " .\n";
				existingCandidate.setCourse(updatedCandidate.getCourse());
			}
			if (updatedCandidate.getSchool() != null) {
				note += "School: from " + existingCandidate.getSchool() + " to " + updatedCandidate.getSchool()
						+ " .\n";
				existingCandidate.setSchool(updatedCandidate.getSchool());
			}
			if (updatedCandidate.getSourceId() != null) {
				note += "Source: from " + existingCandidate.getCandidateSource().getSourceId() + " to "
						+ updatedCandidate.getSourceId() + " .\n";
				CandidateSource source = new CandidateSource();
				source.setSourceId(updatedCandidate.getSourceId());
				existingCandidate.setCandidateSource(source);
			}
			if (updatedCandidate.getJobTitle() != null) {
				note += "Job title: from " + existingCandidate.getJobTitle() + " to " + updatedCandidate.getJobTitle()
						+ " .\n";
				existingCandidate.setJobTitle(updatedCandidate.getJobTitle());
			}
			if (updatedCandidate.getJobType() != null) {
				note += "Job type: from " + existingCandidate.getJobType() + " to " + updatedCandidate.getJobType()
						+ " .\n";
				existingCandidate.setJobType(updatedCandidate.getJobType());
			}
			if (updatedCandidate.getTotalYearsExp() != null) {
				note += "Total year experience: from " + existingCandidate.getTotalYearsExp() + " to "
						+ updatedCandidate.getTotalYearsExp() + " .\n";
				existingCandidate.setTotalYearsExp(updatedCandidate.getTotalYearsExp());
			}
			if (updatedCandidate.getYearGraduated() != null) {
				note += "Year graduated: from " + existingCandidate.getYearGraduated() + " to "
						+ updatedCandidate.getYearGraduated() + " .\n";
				existingCandidate.setYearGraduated(updatedCandidate.getYearGraduated());
			}
			if (updatedCandidate.getYearGraduated() != null) {
				note += "Year graduated: from " + existingCandidate.getYearGraduated() + " to "
						+ updatedCandidate.getYearGraduated() + " .\n";
				existingCandidate.setYearGraduated(updatedCandidate.getYearGraduated());
			}

			if (updatedCandidate.getTechnologies() != null) {
				for (com.xurpas.thirdparty.talent.solutions.payload.UpdateCandidateDto.TechnologiesDto updatedTech : updatedCandidate
						.getTechnologies()) {

					if (updatedTech.getId() != null) {
						// get the row that the user want to be updated
						CandidateTechnology existingTech = candidateTechnologyRepo
								.findCandidateTechnologyById(updatedTech.getId());
						if (existingTech != null) {
							if (updatedTech.getYearExperience() != null) {
								note += "Technology year experience: from " + existingTech.getYearExp() + " to "
										+ updatedTech.getYearExperience() + " . ";
								existingTech.setYearExp(updatedTech.getYearExperience());
							}
							if (updatedTech.getTechnologyId() != null) {
								String existingTechName = technologyRepo
										.getTechnologyById(existingTech.getTechnology().getTechnology_id())
										.getTechnologyName();
								String updatedTechName = technologyRepo.getTechnologyById(updatedTech.getTechnologyId())
										.getTechnologyName();
								note += "Technology name: from " + existingTechName + " to " + updatedTechName + " .\n";
								Technology technology = new Technology();
								technology.setTechnology_id(updatedTech.getTechnologyId());
								existingTech.setTechnology(technology);
							}
							candidateTechnologyRepo.save(existingTech);
						}
					} else {
						// add new technology
						CandidateTechnology newTech = new CandidateTechnology();
						String existingTechName = technologyRepo.getTechnologyById(updatedTech.getTechnologyId())
								.getTechnologyName();

						note += "Technology name: Added new Technology " + existingTechName + " .\n";
						Technology technology = new Technology();
						technology.setTechnology_id(updatedTech.getTechnologyId());
						newTech.setTechnology(technology);
						newTech.setYearExp(updatedTech.getYearExperience());
						existingCandidate.getTechnology().add(newTech);

					}
				}
			}

			if (!newCv.isEmpty()) {
				for (CandidateFile candidateFile : newCv) {
					if (candidateFile.getId() != null) {
						CandidateFile existingFile = fileRepo.getById(candidateFile.getId());
						note += "File:  Updated " + existingFile.getFileType().getType() + " \n";
						existingFile.setFileId(candidateFile.getFileId());
						existingFile.setLink(candidateFile.getLink());
						fileRepo.save(existingFile);
					} else {
						CandidateFile newFileType = new CandidateFile();
						newFileType.setFileId(candidateFile.getFileId());
						newFileType.setFolderId(candidateFile.getFolderId());
						newFileType.setFileType(candidateFile.getFileType());
						newFileType.setLink(candidateFile.getLink());
						existingCandidate.getCandidateFile().add(newFileType);
						note += "File: Added new " + fileTypeRepo.getById(candidateFile.getFileType().getId()).getType()
								+ " \n";
					}

				}
			}
			if (!note.equals("")) {
				CandidateNotes updateInfo = new CandidateNotes();
				updateInfo.setNote(note);
				long emailToId = userAccountRepo.findByEmail(email).get().getUser_id();
				User user = new User();
				user.setUser_id(emailToId);
				updateInfo.setUserAccount(user);
				CandidateStatus updateStatus = new CandidateStatus();
				updateStatus.setStatus_id(statusRepo.findByStatus("update log").getStatus_id());
				updateInfo.setCandidateStatus(updateStatus);
				existingCandidate.getCandidateNotes().add(updateInfo);
			}

			if (updatedCandidate.getNotes() != null) {
				if (updatedCandidate.getNotes().getStatusId() != null) {
					CandidateNotes newNote = new CandidateNotes();
					CandidateStatus status = new CandidateStatus();
					status.setStatus_id(updatedCandidate.getNotes().getStatusId());
					newNote.setCandidateStatus(status);
					newNote.setNote(updatedCandidate.getNotes().getNote());
					long emailToId = userAccountRepo.findByEmail(email).get().getUser_id();
					User user = new User();
					user.setUser_id(emailToId);
					newNote.setUserAccount(user);
					existingCandidate.getCandidateNotes().add(newNote);
					existingCandidate.setCurrentStatus(status);
				}
			}
		}
		candidateRepo.save(existingCandidate);
		return note;
	}

	public GetCandidateProjection getCandidateById(Long id) {
		return candidateRepo.getCandidateById(id);
	}

	public GetCandidateDto getCandidate(Long id) {
		GetCandidateProjection candidate = candidateRepo.getCandidateById(id);
		GetCandidateDto candidateDto = new GetCandidateDto();
		candidateDto.setFirstName(candidate.getFirstName());
		candidateDto.setLastName(candidate.getLastName());
		candidateDto.setMiddleName(candidate.getMiddleName());
		candidateDto.setEmail(candidate.getEmail());
		candidateDto.setMobileNo(candidate.getMobileNo());
		candidateDto.setSource(candidate.getCandidateSource().getSourceName());
		candidateDto.setOwner(candidate.getUserAccount().getFirstName());
		candidateDto.setSchool(candidate.getSchool());
		candidateDto.setJobType(candidate.getJobType());
		candidateDto.setJobTitle(candidate.getJobTitle());
		candidateDto.setTotalYearsExp(candidate.getTotalYearsExp());
		candidateDto.setYearGraduated(candidate.getYearGraduated());
		candidateDto.setCourse(candidate.getCourse());
		candidateDto.setAddress(candidate.getAddress());
		if (candidate.getCurrentStatus() != null) {
			if (candidate.getCurrentStatus().getStatus_id() != 0) {
				String statusRes = statusRepo.getById(candidate.getCurrentStatus().getStatus_id()).getStatus();
				candidateDto.setCurrentStatus(statusRes);
			}
		}

		candidateDto.setDateCreated(candidate.getDateCreated());
		candidateDto.setAskingSalary(candidate.getAskingSalary());
		candidateDto.setCurrentSalary(candidate.getCurrentSalary());
		for (techs c : candidate.getTechnology()) {
			GetTechnologyDto technologyDto = candidateDto.new GetTechnologyDto();
			technologyDto.setId(c.getId());
			technologyDto.setTechnologyId(c.getTechnology().getTechnology_id());
			technologyDto.setTechnologyName(c.getTechnology().getTechnologyName());
			technologyDto.setYearExperience(c.getYearExp());
			candidateDto.getTechnologies().add(technologyDto);
		}
		for (notes n : candidate.getCandidateNotes()) {
			GetNotesDto noteDto = candidateDto.new GetNotesDto();
			noteDto.setStatus(n.getCandidateStatus().getStatus());
			noteDto.setNote(n.getNote());
			noteDto.setUser(n.getUserAccount().getFirstName());
			noteDto.setDateCreated(n.getDateCreated());
			candidateDto.getNotes().add(noteDto);
		}

		for (files f : candidate.getCandidateFile()) {
			FilesDto file = candidateDto.new FilesDto();
			file.setFileId(f.getFileId());
			file.setFolderId(f.getFolderId());
			file.setLink(f.getLink());
			file.setFileType(f.getFileType().getType());
			candidateDto.getFiles().add(file);
		}
		return candidateDto;
	}

}
