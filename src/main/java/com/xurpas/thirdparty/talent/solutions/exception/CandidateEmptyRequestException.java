package com.xurpas.thirdparty.talent.solutions.exception;

import org.springframework.stereotype.Component;

@Component
public class CandidateEmptyRequestException extends RuntimeException {

	private static final long serialVersionUID = 1l;

	private String errorCode;
	private String errorMessage;

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public CandidateEmptyRequestException(String errorCode, String errorMessage) {
		super();
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public CandidateEmptyRequestException() {
		super();
	}

}
