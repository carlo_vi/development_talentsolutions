package com.xurpas.thirdparty.talent.solutions.payload;

import java.util.Date;
import java.util.List;

import com.xurpas.thirdparty.talent.solutions.model.CandidateTechnology;
import com.xurpas.thirdparty.talent.solutions.model.Technology;

public interface TechnologyProjection {

	// Getting Source of Candidate
	String getCandidate_id(); // Asking Salary + Source candidate
	String getDateCreated();
	List<technology> getTechnology();
	//inside model CandidateTechnology
	interface technology{
		int getId();
		tech getTechnology();
		//inside mode technology
		interface tech{
			int getTechnology_id();
		
			String getTechnologyName();
		}
	}

}
