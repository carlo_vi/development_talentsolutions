package com.xurpas.thirdparty.talent.solutions.resource;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xurpas.thirdparty.talent.solutions.exception.ResponseHandler;
import com.xurpas.thirdparty.talent.solutions.model.User;
import com.xurpas.thirdparty.talent.solutions.payload.RecruiterActivitySummaryPaginationDto;
import com.xurpas.thirdparty.talent.solutions.payload.RecruiterTestProjection;
import com.xurpas.thirdparty.talent.solutions.repository.RecruiterRepository;
import com.xurpas.thirdparty.talent.solutions.repository.UserRepository;
import com.xurpas.thirdparty.talent.solutions.service.RecruiterService;

@RestController
@RequestMapping("/")
public class RecruiterResouce {

	@Autowired
	RecruiterService recruiterService = new RecruiterService();
	@Autowired
	UserRepository userRepo;

	@Autowired
	RecruiterRepository recruiterRepo;
	
//	@GetMapping({ "/recruiters" })
//	public ResponseEntity<Object> searchRecruiter(@RequestParam(value = "search", defaultValue = "") String search,
//			@RequestParam(value = "page", defaultValue = "1") Integer page,
//			@RequestParam(value = "size", defaultValue = "10") Integer size,
//			@RequestParam(value = "sortField", defaultValue = "last_activity") String sortField,
//			@RequestParam(value = "sortType", defaultValue = "DESC") String sortType) {
//		RecruiterActivitySummaryPaginationDto res;
//		try {
//
//			res = recruiterService.getPageSearch(search, page, size, sortField, sortType);
//			if (!res.getRecruiters().isEmpty()) {
//				return ResponseHandler.generateResponseWithEntity("Datas successfully retrieved!", HttpStatus.OK, res);
//			} else {
//				return ResponseHandler.generateResponseWithEntity("No record found", HttpStatus.NOT_FOUND, res);
//			}
//		} catch (Exception e) {
//			return ResponseHandler.generateResponse("Bad Request", HttpStatus.BAD_REQUEST);
//
//		}
//
//	}
	@GetMapping({ "/recruiters" })
	public ResponseEntity<Object> getRecruiters(@RequestParam(value = "search", defaultValue = "") String search,
			@RequestParam(value = "page", defaultValue = "1") Integer page,
			@RequestParam(value = "size", defaultValue = "10") Integer size,
			@RequestParam(value = "sortField", defaultValue = "last_login_date") String sortField,
			@RequestParam(value = "sortType", defaultValue = "DESC") String sortType) {
		RecruiterActivitySummaryPaginationDto res;
//		try {

			res = recruiterService.getRecruiter(search, page, size, sortField, sortType);
			if (!res.getRecruiters().isEmpty()) {
				return ResponseHandler.generateResponseWithEntity("Datas successfully retrieved!", HttpStatus.OK, res);
			} else {
				return ResponseHandler.generateResponseWithEntity("No record found", HttpStatus.NOT_FOUND, res);
			}
//		} catch (Exception e) {
//			return ResponseHandler.generateResponse("Bad Request", HttpStatus.BAD_REQUEST);
//
//		}

	}
	
	@GetMapping("/rectest")
	public ResponseEntity<Object> recruiterTest(HttpServletRequest req) {
		
		Pageable pageable = null;
		Page<RecruiterTestProjection> rec=recruiterRepo.testrec(pageable);
		
		if (rec != null) {
			
			return ResponseHandler.generateResponseWithEntity("User FullName successfully retrieved!", HttpStatus.OK, rec);
			
		} else {
			return ResponseHandler.generateResponse("No record found", HttpStatus.NOT_FOUND);
			
		}

	}
	@GetMapping("/recruiter")
	public ResponseEntity<Object> getCurrentLoggedInUser(HttpServletRequest req) {

		String user = req.getUserPrincipal().getName();
		if (user != null) {
			 String res=userRepo.getUserFullNameByEmail(user);
			return ResponseHandler.generateResponseWithEntity("User FullName successfully retrieved!", HttpStatus.OK, res);
			
		} else {
			return ResponseHandler.generateResponse("No record found", HttpStatus.NOT_FOUND);
			
		}

	}
//	@GetMapping({ "/recruiters" })
//	public ResponseEntity<Object> getAll(@RequestParam(value = "page", defaultValue = "1") Integer page,
//			@RequestParam(value = "size", defaultValue = "10") Integer size,
//			@RequestParam(value = "sortField", defaultValue = "last_activity") String sortField,
//			@RequestParam(value = "sortType", defaultValue = "DESC") String sortType) {
//		RecruiterActivitySummaryPaginationDto res;
//		try {
//
//			res = recruiterService.getPage(page, size, sortField, sortType);
//			if (!res.getRecruiters().isEmpty()) {
//				return ResponseHandler.generateResponseWithEntity("Datas successfully retrieved!",
//						HttpStatus.OK, res);
//			} else {
//				return ResponseHandler.generateResponse("No record found",
//						HttpStatus.NOT_FOUND);
//			}
//
//		} catch (Exception e) {
//			return ResponseHandler.generateResponse("Bad Request", HttpStatus.BAD_REQUEST);
//
//		}
//
//	}

}
