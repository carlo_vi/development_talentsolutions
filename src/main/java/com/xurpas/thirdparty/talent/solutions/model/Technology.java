package com.xurpas.thirdparty.talent.solutions.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "technologies")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Technology {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int technology_id;

	@Column(name = "technology_name", length = 50, nullable = false)
	private String technologyName;

	

}
