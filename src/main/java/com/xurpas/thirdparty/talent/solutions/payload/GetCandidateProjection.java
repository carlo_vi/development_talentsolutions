package com.xurpas.thirdparty.talent.solutions.payload;

import java.util.Date;
import java.util.List;

public interface GetCandidateProjection {

	String getFirstName();

	String getMiddleName();

	String getLastName();

	String getMobileNo();

	String getEmail();

	source getCandidateSource();

	interface source {
		String getSourceName();
	}

	currentStatus getCurrentStatus();
	
	interface currentStatus{
		int getStatus_id();
	}
	String getJobTitle();

	String getJobType();

	Integer getTotalYearsExp();

	double getCurrentSalary();

	double getAskingSalary();

	String getCourse();

	String getAddress();

	Date getDateCreated();

	owner getUserAccount();

	interface owner {
		String getFirstName();
	}

	String getSchool();

	Integer getYearGraduated();

	List<techs> getTechnology();

	public interface techs {
		tech getTechnology();

		Integer getYearExp();

		Integer getId();

		interface tech {
			int getTechnology_id();
			String getTechnologyName();

		}

	}

	List<notes> getCandidateNotes();

	public interface notes {

		user getUserAccount();

		status getCandidateStatus();

		String getNote();

		Date getDateCreated();

		interface status {
			String getStatus();
		}

		interface user {
			String getFirstName();
		}

	}

	List<files> getCandidateFile();

	public interface files {
		Integer getId();

		String getFolderId();

		String getFileId();

		String getLink();

		type getFileType();

		interface type {
			String getType();
		}
	}

}
