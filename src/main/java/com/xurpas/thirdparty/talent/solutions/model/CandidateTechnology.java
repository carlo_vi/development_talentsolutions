package com.xurpas.thirdparty.talent.solutions.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "candidate_technology")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CandidateTechnology {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;


	@ManyToOne
	@JoinColumn(name = "technology_id")
	private Technology technology;
	
	private Integer yearExp;

	
	
	
	public CandidateTechnology(Technology technology, Integer yearExp) {
		this.technology = technology;
		this.yearExp = yearExp;
	}

	
}
