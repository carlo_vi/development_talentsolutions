package com.xurpas.thirdparty.talent.solutions.payload;

import java.math.BigInteger;

import lombok.Data;

@Data
public class RecruiterCandidatesTotalDto {
	private BigInteger allTotal;
	private BigInteger allTotalWeek;
}
