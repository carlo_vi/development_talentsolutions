package com.xurpas.thirdparty.talent.solutions.payload;

import java.math.BigInteger;
import java.util.Date;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RecruiterActivitySummaryDto {
	private BigInteger id;
	private String name;
	private BigInteger total_entries_historical;
	private Date last_activity;
	private BigInteger total_entries_this_week;
	


	
}
