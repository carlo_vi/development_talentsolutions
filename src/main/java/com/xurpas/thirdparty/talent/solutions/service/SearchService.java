package com.xurpas.thirdparty.talent.solutions.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.stereotype.Service;

import com.xurpas.thirdparty.talent.solutions.model.TypeOfSearch;
import com.xurpas.thirdparty.talent.solutions.payload.SearchDto;
import com.xurpas.thirdparty.talent.solutions.payload.SearchPaginationDto;
import com.xurpas.thirdparty.talent.solutions.payload.SearchResultProjection;
import com.xurpas.thirdparty.talent.solutions.repository.CandidateRepository;

@Service
public class SearchService {
	@Autowired
	CandidateRepository candidateRepo;

	/*
	 * public SearchPaginationDto advSearch(List<SearchDto> search, Integer page,
	 * Integer size, String sortField, String sortType) {
	 * 
	 * Sort sort = sortType.equalsIgnoreCase("desc") ?
	 * Sort.by(sortField).descending() : Sort.by(sortField).ascending();
	 * 
	 * Pageable pageable = PageRequest.of(page, size, sort);
	 * Page<SearchResultProjection> toPage = null; String status = null; String name
	 * = null; String owner = null; Double askingSalary = null; String[]
	 * technologies = null; String source = null;
	 * 
	 * // for range Double min = null; Double max = null; List<String> test = null;
	 * String searchType = null; String searchField = null;
	 * 
	 * String statusExact = null; String statusBest = null;
	 * 
	 * String nameExact = null; String nameBest = null;
	 * 
	 * String ownerExact = null; String ownerBest = null;
	 * 
	 * String sourceExact = null; String sourceBest = null;
	 * 
	 * String technologyMultiple = null;
	 * 
	 * String askingSalaryExact = null; String askingSalaryRange = null; String
	 * askingSalaryAtleast = null; String askingSalaryMaximum = null;
	 * 
	 * if (!search.isEmpty()) {
	 * 
	 * for (SearchDto val : search) { searchField = val.getField().toLowerCase();
	 * searchType = val.getType().toLowerCase();
	 * 
	 * switch (searchField) { case "status": status = val.getValue().toLowerCase();
	 * switch (searchType) { case "exact_match": statusExact = "1"; break; case
	 * "best_match": statusBest = "1"; break; default: break; }
	 * 
	 * break; case "name": name = val.getValue().toLowerCase(); switch (searchType)
	 * { case "exact_match": nameExact = "1"; break; case "best_match": nameBest =
	 * "1"; break; default: break; } break; case "owner": owner =
	 * val.getValue().toLowerCase(); switch (searchType) { case "exact_match":
	 * ownerExact = "1"; break; case "best_match": ownerBest = "1"; break; default:
	 * break; } break; case "askingsalary": switch (searchType) { case
	 * "exact_match": askingSalaryExact = "1"; askingSalary =
	 * Double.parseDouble(val.getValue()); break; case "range": askingSalary = null;
	 * askingSalaryRange = "1"; String[] rangeArr = val.getValue().split("-"); min =
	 * Double.parseDouble(rangeArr[0]); max = Double.parseDouble(rangeArr[1]);
	 * 
	 * break; case "maximum": askingSalaryMaximum = "1"; askingSalary =
	 * Double.parseDouble(val.getValue()); break; case "atleast":
	 * askingSalaryAtleast = "1"; askingSalary = Double.parseDouble(val.getValue());
	 * break; default: break; } break; case "source": source =
	 * val.getValue().toLowerCase(); switch (searchType) { case "exact_match":
	 * sourceExact = "1"; break; case "best_match": sourceBest = "1"; break;
	 * default: break; } break; case "technology": switch (searchType) { case
	 * "multiple": technologyMultiple = "1"; technologies =
	 * val.getValue().split("\\|"); test = new ArrayList<>(); for (String tech :
	 * technologies) { test.add(tech); } break; default: break; } break;
	 * 
	 * default: break; }
	 * 
	 * } toPage = candidateRepo.adv(statusBest, statusExact, status, ownerBest,
	 * ownerExact, owner, sourceBest, sourceExact, source, nameBest, nameExact,
	 * name, technologyMultiple, test, askingSalaryRange, min, max,
	 * askingSalaryMaximum, askingSalaryAtleast, askingSalaryExact, askingSalary,
	 * pageable); } else { toPage = candidateRepo.blankSearch(pageable); }
	 * 
	 * // normal search
	 * 
	 * List<SearchResultProjection> content = toPage.getContent(); long
	 * totalElements = toPage.getTotalElements(); long totalPages =
	 * toPage.getTotalPages(); int currentPage = toPage.getNumber();
	 * 
	 * return new SearchPaginationDto(content, totalElements, totalPages,
	 * currentPage); }
	 */

	public SearchPaginationDto advSearch123(List<SearchDto> search, Integer page, Integer size, String sortField,
			String sortType) {

		Sort.Direction sort = sortType.equalsIgnoreCase("desc") ? Sort.Direction.DESC : Sort.Direction.ASC;

		Pageable pageable = PageRequest.of(page - 1, size, JpaSort.unsafe(sort, "(" + sortField + ")"));
		Page<SearchResultProjection> toPage = null;
		String status = null;
		TypeOfSearch statusType = null;

		String name = null;
		TypeOfSearch nameType = null;

		String owner = null;
		TypeOfSearch ownerType = null;

		Double askingSalary = null;
		TypeOfSearch askingSalaryType = null;

		String[] technologies = null;
		TypeOfSearch technologyType = null;
		List<String> technologyList = null;

		String[] jobTitles = null;
		TypeOfSearch jobTitlesType = null;
		List<String> jobTitlesList = null;

		String source = null;
		TypeOfSearch sourceType = null;

		// for range
		Double min = null;
		Double max = null;

		String searchField = null;

		if (!search.isEmpty()) {

			for (SearchDto val : search) {
				searchField = val.getField().toLowerCase();

				switch (searchField) {
				case "status":
					status = val.getValue().toLowerCase();
					statusType = val.getType();

					break;
				case "name":
					name = val.getValue().toLowerCase();
					nameType = val.getType();
					break;
				case "owner":
					owner = val.getValue().toLowerCase();
					ownerType = val.getType();
					break;
				case "askingsalary":
					askingSalaryType = val.getType();
					if (askingSalaryType == TypeOfSearch.RANGE) {
						String[] rangeArr = val.getValue().split("-");
						min = Double.parseDouble(rangeArr[0]);
						max = Double.parseDouble(rangeArr[1]);
					} else {
						askingSalary = Double.parseDouble(val.getValue());
					}
					break;
				case "source":
					source = val.getValue().toLowerCase();
					sourceType = val.getType();
					break;
				case "technology":
					technologyType = val.getType();
					if (technologyType == TypeOfSearch.BEST_MATCH) {
						technologyList = new ArrayList<>();
						technologyList.add(val.getValue().toLowerCase());

					} else {
						technologies = val.getValue().toLowerCase().split("\\|");
						technologyList = new ArrayList<>();
						for (String tech : technologies) {
							technologyList.add(tech);
						}
					}

					break;
				case "jobtitle":

					jobTitlesType = val.getType();
					if (jobTitlesType == TypeOfSearch.BEST_MATCH) {
						jobTitlesList = new ArrayList<>();
						jobTitlesList.add(val.getValue().toLowerCase());
					} else {
						jobTitles = val.getValue().toLowerCase().split("\\|");
						jobTitlesList = new ArrayList<>();
						for (String job : jobTitles) {
							jobTitlesList.add(job);
						}
					}

					break;
				default:
					break;
				}

			}
			toPage = candidateRepo.chk(statusType, status, nameType, name, askingSalaryType, askingSalary, min, max,
					ownerType, owner, sourceType, source, technologyType, technologyList, jobTitlesType, jobTitlesList,
					TypeOfSearch.ATLEAST, TypeOfSearch.BEST_MATCH, TypeOfSearch.EXACT_MATCH, TypeOfSearch.MAXIMUM,
					TypeOfSearch.MULTIPLE, TypeOfSearch.RANGE, pageable);
		} else {
			toPage = candidateRepo.findTop50BlankSearch(pageable);
		}

		List<SearchResultProjection> content = toPage.getContent();
		long totalElements = toPage.getTotalElements();
		long totalPages = toPage.getTotalPages();
		int currentPage = toPage.getNumber() + 1;

		return new SearchPaginationDto(content, totalElements, totalPages, currentPage);
	}

	public SearchPaginationDto searchByRecruiterId(Long recruiterId,boolean currentWeek ,Integer page, Integer size, String sortField,
			String sortType) {
		Sort.Direction sort = sortType.equalsIgnoreCase("desc") ? Sort.Direction.DESC : Sort.Direction.ASC;

		Pageable pageable = PageRequest.of(page - 1, size, JpaSort.unsafe(sort, "(" + sortField + ")"));
		Page<SearchResultProjection> toPage = null;
		if (recruiterId != 0) {
			toPage = candidateRepo.getByRecruiterId(pageable, recruiterId,currentWeek);
		}
		List<SearchResultProjection> content = toPage.getContent();
		long totalElements = toPage.getTotalElements();
		long totalPages = toPage.getTotalPages();
		int currentPage = toPage.getNumber() + 1;
		return new SearchPaginationDto(content, totalElements, totalPages, currentPage);
	}
	
	

}
