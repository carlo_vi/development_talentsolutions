package com.xurpas.thirdparty.talent.solutions.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xurpas.thirdparty.talent.solutions.model.CandidateStatus;

@Repository
public interface CandidateStatusRepository extends JpaRepository<CandidateStatus, Integer>{

	CandidateStatus findByStatus(String status);
	
	boolean existsById(Integer id);
}
