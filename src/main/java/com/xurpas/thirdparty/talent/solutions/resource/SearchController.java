package com.xurpas.thirdparty.talent.solutions.resource;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.xurpas.thirdparty.talent.solutions.exception.ResponseHandler;
import com.xurpas.thirdparty.talent.solutions.model.Candidates;
import com.xurpas.thirdparty.talent.solutions.payload.RecruiterActivitySummaryPaginationDto;
import com.xurpas.thirdparty.talent.solutions.payload.SearchDto;
import com.xurpas.thirdparty.talent.solutions.payload.SearchPaginationDto;
import com.xurpas.thirdparty.talent.solutions.payload.SearchResultProjection;
import com.xurpas.thirdparty.talent.solutions.service.SearchService;

@RestController
@RequestMapping("/search")
public class SearchController {
	@Autowired
	SearchService searchService;

	/*
	 * @GetMapping("/candidates") public SearchPaginationDto search(@RequestBody
	 * SearchDto search,@RequestParam(value="page",defaultValue = "0") Integer page,
	 * 
	 * @RequestParam(value="size",defaultValue = "2") Integer size,
	 * 
	 * @RequestParam(value="sortField",defaultValue = "firstName") String sortField,
	 * 
	 * @RequestParam(value="sortType",defaultValue = "DESC") String sortType) {
	 * 
	 * return searchService.search(search, page,size,sortField,sortType);
	 * 
	 * }
	 */

	/*
	 * @GetMapping("/adv") public SearchPaginationDto advSearch(@RequestBody
	 * List<SearchDto> search,@RequestParam(value="page",defaultValue = "0") Integer
	 * page,
	 * 
	 * @RequestParam(value="size",defaultValue = "2") Integer size,
	 * 
	 * @RequestParam(value="sortField",defaultValue = "firstName") String sortField,
	 * 
	 * @RequestParam(value="sortType",defaultValue = "DESC") String sortType) {
	 * 
	 * return searchService.advSearch(search, page,size,sortField,sortType);
	 * 
	 * }
	 */
	@PostMapping("/candidates")
	public ResponseEntity<Object> advSearch(@RequestBody List<SearchDto> search,
			@RequestParam(value = "page", defaultValue = "1") Integer page,
			@RequestParam(value = "size", defaultValue = "10") Integer size,
			@RequestParam(value = "sortField", defaultValue = "dateCreated") String sortField,
			@RequestParam(value = "sortType", defaultValue = "DESC") String sortType) {
		SearchPaginationDto res;

//		try {
			res = searchService.advSearch123(search, page, size, sortField, sortType);
			if (!res.getCandidates().isEmpty()) {
				return ResponseHandler.generateResponseWithEntity("Datas successfully retrieved!", HttpStatus.OK, res);
			} else {
				return ResponseHandler.generateResponseWithEntity("No record found", HttpStatus.NOT_FOUND,res);
			}
//		} catch (Exception e) {
//			return ResponseHandler.generateResponse("Bad request!", HttpStatus.BAD_REQUEST);
//
//		}
	}
	@GetMapping("/recruiter/{recruiterId}/candidates")
	public ResponseEntity<Object> searchByRecruiterId(@PathVariable("recruiterId") Long recruiterId,
			@RequestParam(value = "page", defaultValue = "1") Integer page,
			@RequestParam(value = "size", defaultValue = "10") Integer size,
			@RequestParam(value = "sortField", defaultValue = "dateCreated") String sortField,
			@RequestParam(value = "sortType", defaultValue = "DESC") String sortType,
			@RequestParam(value = "currentWeek", defaultValue = "FALSE") Boolean currentWeek
			){
		SearchPaginationDto res;
		res = searchService.searchByRecruiterId(recruiterId, currentWeek,page, size, sortField, sortType);
		if (!res.getCandidates().isEmpty()) {
			return ResponseHandler.generateResponseWithEntity("Datas successfully retrieved!", HttpStatus.OK, res);
		} else {
			return ResponseHandler.generateResponseWithEntity("No record found", HttpStatus.NOT_FOUND,res);
		}
		
	}
//	@GetMapping("/recruiter/{recruiterId}/candidates")
//	public ResponseEntity<Object> getCandidateByRecruiterIdByCurrentWeek(@PathVariable("recruiterId") Long recruiterId,
//			@RequestParam(value = "page", defaultValue = "1") Integer page,
//			@RequestParam(value = "size", defaultValue = "10") Integer size,
//			@RequestParam(value = "sortField", defaultValue = "candidate_id") String sortField,
//			@RequestParam(value = "sortType", defaultValue = "ASC") String sortType
//			){
//		SearchPaginationDto res;
//		res = searchService.searchByRecruiterId(recruiterId, page, size, sortField, sortType);
//		if (!res.getCandidates().isEmpty()) {
//			return ResponseHandler.generateResponseWithEntity("Datas successfully retrieved!", HttpStatus.OK, res);
//		} else {
//			return ResponseHandler.generateResponseWithEntity("No record found", HttpStatus.NOT_FOUND,res);
//		}
//		
//	}

}
