package com.xurpas.thirdparty.talent.solutions.payload;

import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class AddCandidateDto {
	private String firstName;
	private String lastName;
	private String middleName;
	private String email;
	private String mobileNo;
	private Integer sourceId;
	private String school;
	private String jobType;
	private String jobTitle;
	private Integer totalYearsExp;
	private Integer yearGraduated;
	private String course;
	private String address;
	private Double askingSalary;
	private Double currentSalary;
	private List<TechnologiesDto> technologies ;

	private NotesDto notes ;
	
	private List<FilesDto> files;

	@Data
	public static class NotesDto {
		private Integer statusId;
		private String note;

	}

	@Data
	public static class TechnologiesDto {
		private Integer technologyId;
		private Integer yearExperience;

	}

	@Data
	public static class FilesDto {
		private String path;
		private Integer fileTypeId;
	}

}
