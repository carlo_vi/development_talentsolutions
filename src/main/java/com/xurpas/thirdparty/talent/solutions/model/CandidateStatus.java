package com.xurpas.thirdparty.talent.solutions.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="candidate_status")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CandidateStatus {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int status_id;
	
	@Column(name="status")
	private String status;
	
	public CandidateStatus(int status_id) {
		this.status_id = status_id;
	}
	
	
}
